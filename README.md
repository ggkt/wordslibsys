# wordslibsys

#### 介绍
词库管理系统

#### 运行环境
1、php
2、mysql
3、nginx



#### 安装教程

1.  数据初始化，执行words.sql文件导入到目标库
2.  配置nginx
nginx.conf加入
include conf.d/*.conf;

conf.d目录新建words.conf文件


server {      
        server_name words.cdeledu.com;
       
         root /opt/web/webroot/wordslibsys;
         index  index.html index.htm index.php;
  

         location ~ \.php$ {
                root /opt/web/webroot/wordslibsys;
                fastcgi_pass   127.0.0.1:9000;
                fastcgi_index  index.php;
                fastcgi_param   SCRIPT_FILENAME    $document_root$fastcgi_script_name;
                fastcgi_param   SCRIPT_NAME        $fastcgi_script_name;
                include        fastcgi_params;
        }
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
}

#### 使用说明

1.  启动后访问
http://localhost

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
