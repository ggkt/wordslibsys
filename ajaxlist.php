<?php

session_start();
require_once 'init.php';
require_once 'common.php';

function ajaxmakePage($offset, $limit, $num) {
    $pages = new PageMaker($offset, $limit, "#-OFFSETVALUE", $num, "-OFFSETVALUE");

    return "<div class='showpage'>
    <div> " . $pages->getFirst('首页') . "</div>
    <div> " . $pages->getPre('上一页') . "</div>
    <div> " . $pages->makePage() . "</div>
    <div> " . $pages->getNext('下一页') . "</div>
    <div> " . $pages->getEnd('尾页') . "</div>
    " . $pages->getGoToPage() . "
    </div>";
}

try {
    $common = new common();
    //$common->islogin(); //没有登录跳转到登录页
    $validate = new Validate();
    $wordsList = new WordsList();
    $act = isset($_POST['act'])?$validate->filterVar("post",'act',Validate::$DEFAULT,'无'):(isset($_GET['act'])?$validate->filterVar("get",'act',Validate::$DEFAULT,'无'):null);
   
    if (isset($act)) {
        if ($act == 'logout') {

            $sessid = $_SESSION['user']['sessid'];
            $oauth_login_url = getConfig('oauth_login_url');
            $goto_url = getConfig('goto_url');
            $data['url'] = $oauth_login_url . '/public/login/isLogout.shtm?goto_url=' . $goto_url . '/&sessid=' . $sessid;
            session_destroy();
            echo json_encode($data);
            die;
        }else if ($act == "ywoperate") {
            $id = $validate->filterVar("get", 'id', Validate::$NOTNULL);
            if ($ywoperate->setFlag($id)) {
                $data['status'] = 'y';
            } else {
                $data['status'] = 'n';
            }
            //$data['list'] = $ywoperate->getFlag();
            echo json_encode($data);
        }else if ($act == "checkWord") {
            # code..
            $sisteid = $validate->filterVar("post",'param',Validate::$DEFAULT,'0');
            $wordname = $validate->filterVar("get", 'wordname', Validate::$DEFAULT,0);
            // var_dump($wordname);
            // var_dump($sisteid);
            // exit;
            if($wordsList->getIdByNameSite($wordname, $sisteid)){
                $data['info'] = '该关键词已存在';
                $data['status'] = 'n';
                // var_dump($data);
                // exit;
                echo json_encode($data);
            }else{
                $data['info'] = '该关键词是新的关键词';
                $data['status'] = 'y';
                echo json_encode($data);
            }
        }else if ($act == "checkEditWord") {
            $wordname = $validate->filterVar("post",'param',Validate::$DEFAULT,'0');
            $sisteid = $validate->filterVar("get", 'sisteid', Validate::$DEFAULT,0);
            // var_dump($wordname);
            // var_dump($siteid);
            // exit;
            if($wordsList->getIdByNameSite($wordname, $sisteid)){
                $data['info'] = '该关键词已存在';
                $data['status'] = 'n';
                echo json_encode($data);
            }else{
                $data['info'] = '该关键词是新的关键词';
                $data['status'] = 'y';
                echo json_encode($data);
            }
        }else if ($act == "checkAllWord") {
            # code...
            $checkarr = $validate->filterVar("post",'param',Validate::$DEFAULT,'0');
            $existArr = $wordsList->getIdByName($checkarr);
            if (empty($existArr)) {
                # code...
                $data['info'] = "新的关键词";
                $data['status'] = 'y';
                echo json_encode($data);
               
            }else{
                $data['info'] = "有关键词已存在，请检查";
                $data['status'] = 'n';
                echo json_encode($data);
            }
            //exit;
        }else if ($act =="checkVersion") {
            # code...
            $version = $validate->filterVar("post",'param',Validate::$DEFAULT,'0');
           
            if (is_numeric($version)) {
                # code...
                preg_match('/^[1-9]{1}\d{5}$/',$version,$match);
                if(empty($match)){
                    $data['info'] = "格式:年月，例如：201611";
                            $data['status'] = 'n';
                            echo json_encode($data);exit;
                }

                $data['status'] = 'y';
                echo json_encode($data);
            }else{
                $data['info'] = "版本号不符合要求";
                $data['status'] = 'n';
                echo json_encode($data);
            }
        }
    } else {
        echo 'no act';
    }

} catch (Exception $e) {
    echo json_encode($e->getMessage());
}
?>
