<?php

require_once dirname(__FILE__) . "/init.php";
require_once dirname(__FILE__) . "/common.php";
$sessid = $_GET['sessid'];
$user_name = $_GET['user_name'];

if (!isset($_SESSION)) {
    session_start();
}

if (!empty($sessid)) {
    $_SESSION['user']['sessid'] = $sessid;
    $_SESSION['user']['username'] = $user_name;

    $sqlserver = new Sqlserver();
    //用户信息
    $userinfo = $sqlserver->userinfo($_SESSION['user']['username']);

    if ($userinfo) {
        foreach ($userinfo as $k => $v) {
            $adminID = $v['adminID'];
            $ruls[] = $v['roleID'];
        }
    }

    $_SESSION['user']['adminID'] = $adminID;
    $ruls = implode(',', $ruls);
    $_SESSION['user']['ruls'] = $ruls;
    //角色名存入SESSION
    if (!empty($_SESSION['user']['ruls'])) {
        $sys = getConfig('systemType');
        $res_arr = $sqlserver->roleinfo("roleID in ($ruls) and systemID = {$sys}","roleName");
        $_SESSION['user']['rolename'] = $res_arr["roleName"];
    }

    header('Location:index.php');
} else {
    header('Location:' . getConfig('goto_url') . '/login.php');
}
?>




