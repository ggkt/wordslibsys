<link rel="stylesheet" href="/../css/common.css" />
<?php 
require_once dirname(__FILE__).'/../../init.php';
require_once '../../PHPExcel/Classes/PHPExcel.php';
require_once '../../PHPExcel/Classes/PHPExcel/IOFactory.php';
require_once '../../PHPExcel/Classes/PHPExcel/Reader/Excel5.php';
require_once '../../PHPExcel/Classes/PHPExcel/Writer/Excel5.php';
function getArrtoArr($uploadfile,$ftype,$version,$sen_type,$sen_level,$sisteid,$systype,$iswhitelist)
{
	if ($ftype == "xls"){		
		$objReader = PHPExcel_IOFactory::createReader('Excel5');//use excel2007 for 2007 format
	}else {
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');//use excel2007 for 2007 format
	}
	$objPHPExcel = $objReader->load($uploadfile); 

	$sheet = $objPHPExcel->getSheet(0); 
	$highestRow = $sheet->getHighestRow(); // 取得总行数 
	$highestColumn = $sheet->getHighestColumn(); // 取得总列数
	$arr_result=array();
	

	for($j=1;$j<=$highestRow;$j++)
	{ 
		if(isset($strs))
		{
			unset($strs);
		}
		$strs="";
	 	for($k='A';$k<= $highestColumn;$k++)
	    { 
		     //读取单元格
		 	 $strs  .= $objPHPExcel->getActiveSheet()->getCell("$k$j")->getValue().'``';
	    }	
	    //return $strs; 
	    $arr_result_tmp = explode("``",$strs);
	    if (empty($arr_result_tmp[0])){
	    	continue;
	    }

	    $arr_result[$j-1]['word'] = $arr_result_tmp[0];
	    $arr_result[$j-1]['version'] = $version;
	    $arr_result[$j-1]['sen_level'] = $sen_level;
	    $arr_result[$j-1]['sen_type'] = $sen_type;
	    $arr_result[$j-1]['sisteid'] = $sisteid;
	    $arr_result[$j-1]['systype'] = $systype;
	    $arr_result[$j-1]['iswhitelist'] = $iswhitelist;
	}
	
	return $arr_result;
}
try{	
	$wordslist = new WordsList();
	$validate = new validate();
	if(isset($_FILES['wordFile']))
	{
		$version = trim($validate->filterVar("post", "version",Validate::$DEFAULT,''));
		$sen_type = trim($validate->filterVar("post",'sen_type',Validate::$DEFAULT,''));
		$sen_level = trim($validate->filterVar("post",'sen_level',Validate::$DEFAULT,''));
		
		$sisteid = trim($validate->filterVar("post",'sisteid',Validate::$DEFAULT,''));
		$systype = trim($validate->filterVar("post",'systype',Validate::$DEFAULT,''));
		$iswhitelist = trim($validate->filterVar("post",'iswhitelist',Validate::$DEFAULT,''));

		$file=$_FILES['wordFile'];
		
		$fname=$file['name'];
		$ftype=strtolower(substr(strrchr($fname,'.'),1));
		$uploadfile=$file['tmp_name'];

		$upfile = $_FILES['wordFile'];
		if($ftype == 'xls' || $ftype == 'xlsx')
		{
			$rs_arrs = getArrtoArr($uploadfile,$ftype,$version,$sen_type,$sen_level,$sisteid,$systype,$iswhitelist);
// 			var_dump($rs_arrs);
// 			exit;
			$wordslist->batchInput($rs_arrs);
			// $rs_str = serialize($rs_arrs);
			
		}else{
			echo "<script type='text/javascript'>alert('文件格式不对！');</script>";
		}
	}

	
}catch(Exception $e)
{
	echo '<center><h1><font color="red">程序出错了，请查看日志！</font></h1></center>';
	Debug::writeLogs($e->getMessage()) ;
}
?>
<div class = "BatchAdd" >
		<font color="red">注：1.文件格式只支持xls、xlsx文件<br>2.格式如：敏感词</font><br><br>
		
		<form class="wordBatchAddForm" method = "post" action="" enctype="multipart/form-data">
		    版本号：<input type="text" name="version" id="version" value="" />&nbsp;&nbsp;&nbsp;
				
				级别：
					<select name = 'sen_level' id = 'sen_level'>

						<?php
							$typearr = getConfig('sen_level'); 
							foreach ($typearr as $key => $value): ?>
							<option <?php if($value == $sen_level) {echo "selected = selected";}?> value = "<?php echo $key;?>"><?=$value;?></option>
						<?php endforeach;?>
					</select>
				&nbsp;&nbsp;&nbsp;
				类型：
					<select name = 'sen_type' id = 'sen_type'>
					
						<?php
							$typearr = getConfig('sen_type'); 
							foreach ($typearr as $key => $value): ?>
							<option <?php if($value == $sen_type) {echo "selected = selected";}?> value = "<?php echo $key;?>"><?=$value;?></option>
						<?php endforeach;?>
					</select>
				&nbsp;&nbsp;&nbsp;
				网站：
					<select name = 'sisteid' id = 'sisteid'>
					
					<?php 
						$sisteidarr = getConfig('sisteid');foreach($sisteidarr as $key=>$value):?>
					<option <?php if ($value == $sisteid) {
						echo "selected = selected";
					}?> value="<?php echo $key;?>"><?php echo $value;?></option>
					<?php endforeach;?>
					</select>
				&nbsp;&nbsp;&nbsp;<br /><br />
				系统：
					<select name = 'systype' id = 'systype'>
						
						<?php $systypearr = getConfig('systype');foreach($systypearr as $key => $value):?>
						<option <?php if($value == $systype){echo "selected = selected";}?> value="<?php echo $key;?>"><?php echo $value;?></option>
					<?php endforeach;?>
					</select>
				&nbsp;&nbsp;&nbsp;
				白名单：
					<select name = 'iswhitelist' id = 'iswhitelist'>
						
						<?php $iswhitelistarr = getConfig('iswhitelist');foreach($iswhitelistarr as $key => $value):?>
						<option <?php if($value == $iswhitelist){echo "selected = selected";}?> value="<?php echo $key;?>"><?php echo $value;?></option>
					<?php endforeach;?>
					</select>
	  		<input type="file" name="wordFile" value="" id="wordFile" />
	  		<input type="submit" value="上传" id="submit" />
	  		
	 		<p style="color: red;">上传文件格式如下图：
	 		只需要将要上传的敏感词放在一列</p>
	 		<img src="../../images/shili.png" style="margin:0px auto;width:60%;">
	 	
	 	</form> 	
</div>

