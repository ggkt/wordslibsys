<?php
try{
	if ($wordname = $_GET['wordname']) {
		require_once dirname(__FILE__)."/../../init.php";

		$wordslist = new WordsList();
		$validate = new Validate();
		$checkarr =array();
		$wordname = trim($validate->filterVar('get', 'wordname',Validate::$DEFAULT,''));
		// $version = trim($validate->filterVar('get','version',Validate::$DEFAULT,''));
		$siteid = $validate->filterVar('get', 'siteid',Validate::$DEFAULT,'');
		$sen_type = $validate->filterVar('get', 'sen_type',Validate::$DEFAULT,0);
		$offset = trim($validate->filterVar('get','offset',Validate::$DEFAULT,''));
		$act = $validate->filterVar('get', 'act',Validate::$DEFAULT,'');
		$pageParam = $validate->filterVar('get', 'str', Validate::$DEFAULT, '');
		if ($sen_type == '3') {
			$checkarr = array(
				"content" => $wordname,
				"rtnword" => 'true',
				"veronly" => 'true',
				"searchall" => 'true',
				"isadv" => 'true',
				"site"=> $siteid,
			);
		}else{
			$checkarr = array(
				"content" => $wordname,
				"rtnword" => 'true',
				"veronly" => 'true',
				"searchall" => 'true',
				"isadv" => 'false',
				"site"=> $siteid,
			);
		}
		
		$str = "";
		$str .= http_build_query($checkarr);
		
		//$url = 'http://filter.chnedu.com/v2/SensitiveServlet';
		$url = getConfig('select_url');
		$ch = curl_init();//初始化curl
		curl_setopt($ch,CURLOPT_URL,$url);//抓取指定网页
 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);//超时时间
		curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
		curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
		$data = curl_exec($ch);//运行curl
		curl_close($ch);
		$result = json_decode($data,true);

 		$status = $result['returnCode'];
		//$word = $result['sensitiveWords']['0']['value'];
		$word = $checkarr['content'];
		
		if($status == 1){
			echo "<script>alert('$word 已存在');</script>";
			echo "<script>window.location.href='../view/index.php?offset=$offset&act=$act&$pageParam';</script>";
		}elseif ($status == 0){
			echo "<script>alert('敏感词不存在');</script>";
			echo "<script>window.location.href='../view/index.php?offset=$offset&act=$act&$pageParam';</script>";
	}else{
		echo "数据获取异常！！！";
	}
}
}catch(Exception $e){
	echo "<center><h1><font color='red'>程序出错了，请查看日志！</font></h1></center>";
	Debug::writeLogs($e->getMessage());
	
}

?>