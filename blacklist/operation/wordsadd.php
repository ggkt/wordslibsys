<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" href="../../css/common.css" />
    
    <script src="../../js/jquery-1.4.2.min.js"></script> 
<!--    <script src="../../js/jquery-1.6.2.min.js"></script>  -->
    <script src="../../js/Validform_v5.3.2.js"> </script>
    <script type="text/javascript">
    $(function(){
        $("#sisteid").change(function(){
            wordname = $("input[name='wordsname']").attr("value");
            //alert(wordname);
            $("#sisteid").attr("ajaxurl","../../ajaxlist.php?act=checkWord&wordname="+wordname);
        });
        $(".userAddForm").Validform({
          tiptype:2
       });
    })
    </script>
</head>
<?php 
header('Content-type: text/html; charset=UTF-8');
require_once dirname(__FILE__).'/../../init.php';
try{    
    $wordslist = new WordsList();
    $act = "add";
    
    $log = new Log();
    
    $table = 'words';
    $addsystype = '';
    $addtype = '';
    $addsisteid = '';
    $addlevel = '';
    // echo $_POST['type'];
    // echo 
    if(isset($_POST['wordsname']))
    {   
        $validate = new Validate();
        if(isset($_POST['wordsname']) && isset($_POST['version']) && isset($_POST['sen_type']) && isset($_POST['sisteid']) && isset($_POST['systype']) && isset($_POST['sen_level']))
        {
            $addArr = array();
            $addArr['table'] = $table;
            $addArr['word'] = trim($validate->filterVar("post",'wordsname',Validate::$DEFAULT,''));
           
            $addArr['version'] = trim($validate->filterVar("post",'version',Validate::$DEFAULT,''));
            $addArr['sen_level'] = trim($validate->filterVar("post", 'sen_level',Validate::$DEFAULT,''));
            $addArr['sen_type'] = trim($validate->filterVar("post",'sen_type',Validate::$DEFAULT,''));
            $addArr['sisteid'] = trim($validate->filterVar("post",'sisteid',Validate::$DEFAULT,''));
            $addArr['systype'] = trim($validate->filterVar("post",'systype',Validate::$DEFAULT,''));
            $addArr['iswhitelist'] = 0;
            $res = $wordslist->add($addArr,$act);
            if($res)
            {
            	echo "<script>alert('添加成功');</script>";
            	echo "<script>window.location.href='../view/index.php';</script>";
            }
            else
            {
                echo "<script type='text/javascript'>alert('添加失败');</script>";
            }
        }
    }
}catch(Exception $e)
{
    echo '<center><h1><font color="red">程序出错了，请查看日志！</font></h1></center>';
    Debug::writeLogs($e->getMessage()) ;
}
?>

<div class = "wordsadd" >
    <form class="userAddForm" method = "post" action="wordsadd.php">
            <table width="100%" style="table-layout:fixed;">
                 <tr>
                    <td class="need">*</td>
                    <td>关键词：</td>
                    <td style="width:150px;"><input type="text" value="" name="wordsname" class="inputxt" datatype="*" errormsg="关键词不能为空" /></td>
                    <td><div class="Validform_checktip">多个关键词以|分割</div></td>
                </tr>
                <tr>
                    <td class = "need">*</td>
                    <td>版本号：</td>
                    <td><input type = "text" value = "" name = "version" class = "inputxt" datatype = "*" nullmsg = "请填写版本号" ajaxurl = "../../ajaxlist.php?act=checkVersion" /></td>
                    <td><div class="Validform_checktip">不能为空，参考201601</div></td>
                </tr>
                <tr>
                    <td class = "need">*</td>
                    <td>敏感级别：</td>
                    <td>
                        <select name = 'sen_level' id = 'sen_level' class = 'select'>
                            <?php $levelarr = getConfig('sen_level');
                                foreach($levelarr as $key => $value):?>
                                <option <?php if ($value == $addlevel){echo "selected = selected";}?> value = "<?php echo $key;?>"><?php echo $value;?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="need">*</td>
                    <td>类型：</td>
                    <td>
                        <select name = 'sen_type' id = 'sen_type' class = 'select'>
                            <?php $typearr = getConfig('sen_type');
                            foreach($typearr as $key => $value):?>
                            <option <?php if($value == $addtype){echo "selected = selected";}?> value="<?php echo $key;?>"><?php echo $value;?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>     
                <tr>
                    <td class="need">*</td>
                    <td>网站：</td>
                    <td>
                        <select name = 'sisteid' id = 'sisteid' class = 'select' datatype = "*">
                            <option value=" ">请选择</option>
                            <?php $sisteidarr = getConfig('sisteid');
                            foreach($sisteidarr as $key => $value):?>
                            <option <?php if($value == $addsisteid){echo "selected = selected";}?> value="<?php echo $key;?>"><?php echo $value;?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                    <td><div class="Validform_checktip"></div></td>
                </tr>
                <tr>
                    <td class="need">*</td>
                    <td>系统：</td>
                    <td>
                        <select name = 'systype' id = 'systype' class = 'select'>
                            <?php $systypearr = getConfig('systype');
                            foreach($systypearr as $key => $value):?>
                            <option <?php if($value == $addsystype){echo "selected = selected";}?> value="<?php echo $key;?>"><?php echo $value;?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="need"></td>
                    <td></td>
                    <td colspan="2" style="padding:10px 0 18px 0;">
                        <input type="submit" value="提 交" /> <input type="reset" value="重 置" />
                    </td>
                </tr>
            </table>
    </form>
 </div>

 </html>
