<?php
/*
 *定义文件导入操作类
 *
 */
class BatchInput extends Common {
    private $db;
    private $file;
    private $fname;
    private $ftype;
    private $uploadfile;
    //构造函数
    function __construct($f =""){
        
        $this->db = new DbModel();
        //$this->file = $_FILES[$f];   
    }
    //文件类型判断函数
   public function determineType($file){
       
           // $file=$this->File;
            $this->file = $file;
            $this->fname= $this->file['name'];
            $this->ftype=strtolower(substr(strrchr($this->fname,'.'),1));
            return  $this->fname;
            exit;
            $this->uploadfile=$this->file['tmp_name'];
            if($this->ftype == 'xls'||$this->ftype == 'xlsx')
            { 
                return $this->getArrtoArr($this->uploadfile);
            }
            else{
                echo"<script type ='text/javascript'>alert('文件格式不正确');</script>";
            }
    
   }
    //扫描Excel表格
   function getArrtoArr($loadfile)
    {
        $i=0;       //定义一个变量 i
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel = $objReader->load($loadfile);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();       //获取列数
        $highestColumn = $sheet->getHighestColumn();   //获取行数
        $arr_result=array();
        // $server =array();
        for($j=1;$j<=$highestRow;$j++)
	{ 
		$strs="";
	 	for($k='A';$k<= $highestColumn;$k++)
	    { 
		     //读取单元格
		 	 $strs  .= $objPHPExcel->getActiveSheet()->getCell("$k$j")->getValue().'``';
	    }	 
	    $arr_result_tmp = explode("``",$strs);
	    $arr_result[$j-1]['ip'] = $arr_result_tmp[0];
	    $arr_result[$j-1]['domain'] = $arr_result_tmp[1];
	    $arr_result[$j-1]['jbosspath'] =$arr_result_tmp[2];
	    $arr_result[$j-1]['propath'] = str_replace('|','\n',$arr_result_tmp[3]);
	}
        return $arr_result;
    } 
    
	/**
	*	批量导入
	*/
	public function batchInput($se_arr = array())
	{
            $number=1;
		if(!empty($se_arr))
		{
			// $serdata = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'",$serdata );			
			// $ps_arr = unserialize($serdata);
			foreach($se_arr as $key)
			{
				$res = $this->inputOne($key['ip'],$key['domain'],$key['jbosspath'],$key['propath']);
				if($res == 1)
				{
					echo $number++."、".$key['ip']."&nbsp;&nbsp;".$key['domain']."&nbsp;&nbsp;".$key['jbosspath']."&nbsp;&nbsp;".$key['propath']."---添加成功！<br>";
				}
				elseif($res == 0)
				{
					echo $number++."、".$key['ip']."&nbsp;&nbsp;".$key['domain']."&nbsp;&nbsp;".$key['jbosspath']."&nbsp;&nbsp;".$key['propath']."---<font color='red'>添加失败！</font><br>";
				}
				elseif ($res == 2) {
					echo $number++."、".$key['ip']."&nbsp;&nbsp;".$key['domain']."&nbsp;&nbsp;".$key['jbosspath']."&nbsp;&nbsp;".$key['propath']."---<font color='red'>添加失败！原因：该帐号已添加！</font><br>";
				}
				
			}
		}
		return false;		
	}
	private function inputOne($ip="",$domain="",$jbosspath="",$propath="")
	{	$this->logtype = 3;	
		$createtime = date('Y-m-d H:i:s',time());
		//$expiretime = date('Y-m-d H:i:s', strtotime("$createtime +1 month")); 
		$userid = $_SESSION['user']['id'];
		if($res = $this->ser_exist($ip,$domain,$propath,$jbosspath))
		{
			return 2;
		}
		$sql = "insert into server (ip,domain,userid,createtime,jbosspath,propath)values('{$ip}','{$domain}',$userid,'{$createtime}','{$jbosspath}','{$propath}')";
		if($this->db->execute($sql))
		{       
			$record = '添加了一条服务器记录：'.$ip.'下的域名为'.$domain;
			Operatelog::add($this->db,$this->getNowUser(),$this->logtype,$record,$this->getNowIp());						
			return 1;
		}	
		return 0;		
	}

	/**
	*	通过ip 字段来查询是否存在该ip，若ip存在判断查询到的值是否和$domain值是否相等，若相等更新domain字段里的内容
	*/
	function ser_exist($ip,$domain,$propath,$jbosspath)
	{
		$sql = "select id from server where ip='{$ip}'and domain='{$domain}'and propath='{$propath}' and jbosspath = '{$jbosspath}'";
		if($rs = $this->db->find($sql))
		{
			return $rs;
			
		}
		return false;
	}
    
    
    
    
    
    
    
}



