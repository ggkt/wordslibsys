<?php
    // 批量导出公共操作类
class BatchOutput extends Common{
    private $rPHPexcel;
    private $wordslist;
    private $outputFileName;
    private $i = 2; //设置i的初始值
    //设置构造函数
    function __construct(){     
        $this->rPHPexcel = new PHPExcel();  //实例化phpexcel对象
        $this->wordslist = new WordsList();  //实例化Wordlist实例
        $this->outputFileName = 'passwordlist.xls';//设置导出文件名
    }
    //设置导出函数
    function batchoutput($iswhitelist){
        $this->rPHPexcel->getActiveSheet()->setCellValue('A1', '关键词'); 
        $this->rPHPexcel->getActiveSheet()->setCellValue('B1', '版本号'); 
        $this->rPHPexcel->getActiveSheet()->setCellValue('C1', 'type'); 
	$this->rPHPexcel->getActiveSheet()->setCellValue('D1', 'sisteid'); 
        $this->rPHPexcel->getActiveSheet()->setCellValue('E1', 'systype'); 
        if($this->u->isAdmin($uid)){
        $data = $this->p->pslistisAdmin(); //若是超级用户则返回所有密码
        }else{
        	$order = 'updatetime DESC';
        	if($psArr = $this->perm->getPsidByUserid($uid)){
        	$data = $this->p->passwdlist($order,0,0,false,$uid,$psArr);//返回普通用户可操作的密码
        }else {
        	$data = '';
        }}
        
        foreach($data as $item){ 
            $this->rPHPexcel->getActiveSheet()->setCellValue('A' . $this->i, $item['serverip']); 
            $this->rPHPexcel->getActiveSheet()->setCellValue('B' . $this->i, $item['username']); 
            $this->rPHPexcel->getActiveSheet()->setCellValue('C' . $this->i, $item['passwd_decode']);
	    $this->rPHPexcel->getActiveSheet()->setCellValue('D' . $this->i, $item['passwdtype']);
            $this->rPHPexcel->getActiveSheet()->setCellValue('E' . $this->i, $item['note']); 
            $this->i ++; 
        }
        $xlsWriter = new PHPExcel_Writer_Excel5($this->rPHPexcel); 
        header("Content-Type: application/force-download"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Type: application/download"); 
        header('Content-Disposition:inline;filename="'.$this->outputFileName.'"'); 
        header("Content-Transfer-Encoding: binary"); 
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
        header("Pragma: no-cache"); 
        $xlsWriter->save( "php://output" );
        return true;
    }
    
}



