<?php

/**
 * 公共操作类
 *
 */
class Common {

    /**
     * 构造函数
     * 判断用户是否登录
     *
     */
    private $srlkey = "1m5pY2tuYW2iMTAwf3YTozOntzOjg6I1lIjtzOjU6ImFkbWluIjtzOjQ6InBhc3MiO3M6NDoCI7czo0OiJzYXZlIjtiOjE7fQ";

    function __construct() {
        
        date_default_timezone_set('PRC');
    }

    public function islogin() {

        if (!isset($_SESSION)) {
            session_start();
        }
        //$user = new User();

        if (!isset($_SESSION['user'])) {

            $url = getConfig('goto_url') . '/login.php';
            header("location:$url");
        }
    }

    public function saveCookiePwd($user) {
        $strpass = $user['passwd'];

        if (trim($strpass) != "") {
            $key = $this->srlkey;
            $tmppass = "";
            for ($i = 0; $i < strlen($strpass); $i++) {
                $tmpord = ord($strpass{$i}) ^ ord($key{$i});
                $tmppass .= $tmpord . "|";
            }
            $user['passwd'] = $tmppass;
        }
        $srluser = base64_encode(serialize($user));
        // setcookie("USEME", "", time()-60*60*20, "/",$this->getHost());
        // setcookie("USEME",$srluser, time()+3600*24*5, "/",$this->getHost()); 
        setcookie("USEME", $srluser, time() - 1);
        setcookie("USEME", $srluser, time() + 3600 * 24 * 5);
    }

    public function getCookiePwd($usercookie, $depass = true) {
        if (trim($usercookie) == "")
            return "";

        $key = $this->srlkey;
        $user = unserialize(base64_decode($usercookie));

        if (!$depass)
            return $user;
        else {
            $tmppass = $user['passwd'];
            if (trim($tmppass) != "") {
                $strcode = explode("|", $tmppass);
                $pass = "";
                for ($i = 0; $i < count($strcode) - 1; $i++) {
                    $tmp = $strcode[$i] ^ ord($key{$i});
                    $pass.=chr($tmp);
                }
                $user['passwd'] = $pass;
            }
            return $user;
        }
    }

    public function getHost() {
        $host = explode(":", $_SERVER['HTTP_HOST']);
        return $host[0];
    }

    /**
     * 生成随机字符串
     */
    public function getRandChar($length) {
        $str = null;
        $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz~@#$%^&*(){}[]|";
        $max = strlen($strPol) - 1;

        for ($i = 0; $i < $length; $i++) {
            $str.=$strPol[rand(0, $max)]; //rand($min,$max)生成介于min和max两个数之间的一个随机整数
        }
        return $str;
    }

    /**
     * 加密解密
     */
    public function makekey($keys, $index = false) {
        if (!$index) {
            $keyslen = strlen($keys);
            $randindex = rand(1, $keyslen - 1);
            return array("index" => $randindex, "key" => $keys{$randindex});
        } else {
            return $keys{$index};
        }
    }

    public function encode($text, $keystr) {
        $tmpstr = "";

        for ($i = 0; $i < strlen($text); $i++) {
            $keys = $this->makekey($keystr);
            $key = $keys['key'];
            $index = $keys['index'];
            $char = $text{$i};

            $tmpord = ord($char) ^ ord($key);

            $tmpstr .= $tmpord . "." . $index . "|";
        }

        return base64_encode($tmpstr);
    }

    public function decode($text, $keystr) {
        $tmpstrs = explode("|", base64_decode($text));
        $ret = "";

        for ($i = 0; $i < count($tmpstrs); $i++) {
            if ($tmpstrs[$i] != "") {
                $tmpkeys = explode(".", $tmpstrs[$i]);
                $index = $tmpkeys[1];
                $str = $tmpkeys[0];
                $key = $this->makekey($keystr, $index);
                $ret .= chr($str ^ ord($key));
            }
        }

        return $ret;
    }

    public function getNow() {
        return date('Y-m-d H:i:s', time());
    }

    public function getNowUser() {
        return $_SESSION['username'];
    }

    public function getNowIp() {
        return $_SERVER['REMOTE_ADDR'];
    }

}

?>
