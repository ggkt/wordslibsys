<?php
require_once dirname(__FILE__)."/../common.php";

class DbManager
{	

    private static $instance = null;

    public static function getInstance($db_type = 'mysql') {
       
        if ($db_type == 'mysql') {
            if (self::$instance == null) {
                $configArr = getConfig('mysql');
               self::$instance = new PDO("mysql:host={$configArr['DB_HOST']};port={$configArr['DB_PORT']};dbname={$configArr['DB_NAME']}","{$configArr['DB_USER']}","{$configArr['DB_PASS']}"); 
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$instance->exec("set names utf8");
            }
    
        }elseif ($db_type == 'Sqlserver') {

            if (self::$instance == null) {
                $configArr = getConfig('Sqlserver');
                self::$instance = new PDO("dblib:host={$configArr['DB_HOST']};dbname={$configArr['DB_NAME']}","{$configArr['DB_USER']}","{$configArr['DB_PASS']}");
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                
            }
        }
        return self::$instance;
    }

}


?>
