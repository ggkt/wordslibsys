<?php
/**
 * class.debug.php
 * @author  ziven
 */
 class Debug extends Common
 {
 	private $mode;
 	public function __construct()
 	{
 		parent::__construct();
 	}
 	
 	public static function writeLogs($content)
 	{
	 	$file = dirname(__FILE__)."/../codelogs/".date("Ymd").".log";
	 	$now = date('Y-m-d H:i:s',time());
	 	if(!file_exists($file))
		{
			touch($file);
			chmod($file,0664);
		}
		$content = "\n".$now.'：'.$content;
		if (is_writable($file)) {
			$f = fopen($file, 'a+');
			fwrite($f, $content);
			fclose($f);
		} else {
		   echo '日志文件权限不够';
		}
 	}
 	public static function printInfo($e)
 	{
 		echo "<pre>";
 		print_r($e);
 		echo "</pre>";
 	}
 	
 }
?>
