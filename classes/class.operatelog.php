<?php
/**
 * class.peratelog.php
 * @author  ziven
 * @package cdelzone
 */
 class Operatelog extends Common
 {
 	private $optype;
 	private $db;
 	private $id;
 	private $opuid;
 	public  $opuser;
 	private $opcontent;
 	private $optime;
 	public $types = array(
 		1 => "用户操作",
	 	2 => "密码操作",
	 	3 => "服务器操作",
	 	4 => "权限操作",
	 	99 => "不限",
 	);
 	
 	public function getContent()
 	{
 		$ret = '';
 		$c = explode('|', $this->opcontent);
 		switch ($this->getOptype()) 
 		{
 			case 1: 
 				try {
 					$topic = $this->getTopicByPostid($c[0]);			
 					$ret .= "修改帖子： 主题<a href='../viewtopic.php?topicid={$topic['topicid']}' target='_blank'>{$topic['title']}</a><br/>";
 					$ret .= "[<a href='?act=postcompare&id=".$this->getId()."' target='_blank'>查看修改情况</a>]";
 				} catch (Exception $e) {
 					$ret .= "找不到主题";
 				}
 				break;
 			case 2: 
 				try {
 					$post = Post::getPost($this->dbm, $c[0]); 			
 					$summary = $this->subText(strip_tags($post->getcontent()));
 					$ret .= "删回帖 <a href='?act=viewpost&postid={$c[0]}' target='_blank'>{$summary}</a>";
 					$ret .= "<br />[<a href='?act=recoverpost&postid={$c[0]}' target='_blank'>恢复</a>]";
 				} catch (Exception $e) {
 					$ret .= "找不到帖子";
 				}
 				break;
 			case 3: 
 				try {
	 				$topic = $this->getTopic($c[0]);
	 				$type = ($c[1]>0)?'全站':'本版';//0为全站置顶,其它为siteid
	 				$ret .= "{$c[2]}级{$type}置顶<a href='../viewtopic.php?topicid={$topic['topicid']}' target='_blank'>{$topic['title']}</a>";
 				} catch (Exception $e) {
 					$ret .= "找不到帖子";
 				}		
 				break;
 			case 4: 
 				$topic = $this->getTopic($c[0]);
 				$ret .= "撤销置顶<a href='../viewtopic.php?topicid={$topic['topicid']}' target='_blank'>{$topic['title']}</a>";		
 				break;
 			case 5: 
 				$profile = UserProfile::getUserProfile($this->dbm, $c[0]); 				
 				try {
 					$forum = Forum::getForum($this->dbm, $c[2]);
 					$forumname = $forum->getTitle();
 				} catch (Exception $e) {
 					$forumname = $c[2];
 				}
 				$type = $c[1]==0?'版主':($c[1]==1?'实习版主':'嘉宾');
 				$ret .= "委任<a href='../viewprofile.php?uid={$c[0]}' target='_blank'>".$profile->getNickname()."</a>为{$forumname}{$type}";		
 				break;
 			case 6: 
 				$profile = UserProfile::getUserProfile($this->dbm, $c[0]); 
 				try{	
 				$forum = Forum::getForum($this->dbm, $c[1]);
 				$forum = " [".$forum->getTitle()."] "	;
 				}
 				catch(Exception $e)
 				{
 					$forum = "";
 				}
 				$ret .= "撤销".$forum."<a href='../viewprofile.php?uid={$c[0]}' target='_blank'>".$profile->getNickname()."</a>版主";		
 				break;
 			case 7: 
 				$topic = $this->getTopic($c[0]);
 				$ret .= "设置精华<a href='../viewtopic.php?topicid={$c[0]}' target='_blank'>".$topic['title']."</a>";		
 				break;
 			case 8: 
 				$topic = $this->getTopic($c[0]);
 				$ret .= "删除精华<a href='../viewtopic.php?topicid={$c[0]}' target='_blank'>".$topic['title']."</a>";		
 				break;
 			case 9: 
	 			$post = Post::getPost($this->dbm, $c[0]);
 				$summary = $this->subText($post->getcontent());
 				$ret .= "审核 <a href='?act=viewpost&postid={$c[0]}' target='_blank'>{$summary}</a>";		
 				break;
 			 				
 			
 			default:	
 				$ret .= $this->getTypeName()." ".$this->opcontent;
 				break;
 		}
	
 		return $ret;
 	}
 	
 	public function __construct()
 	{
 		$this->db = new DbModel();
 	}
 	
 	public static function add($dbm,$user,$type,$record,$ip)
 	{
 		$now = date('Y-m-d H:i:s',time());
		$sql = "insert into logs (type,user,record,time,ip) " .
				"values($type ,'{$user}','{$record}','{$now}','{$ip}')";
 		if($dbm->execute($sql))
 		{
 			return true;
 		}
 		else
 		{
 			throw new Exception("Operatelog add error!");		
 		}
 	}
 	public function logList($order,$limit,$offset)
 	{
 		$sql = "select * from logs";
 		if(!empty($order))
		{
			$sql .= " order by {$order}";
		}
		if(isset($offset) && isset($limit))
		{
			$sql .= " limit $offset,$limit";				
		}

		if($res = $this->db->findAll($sql))
		{
			return $res;
		}
		else
		{
			return false;			
		}
 	}
 	
 	public function getBy_($opType = '0',$opcontent = '0',$auther = '0')
 	{
 		$sql = "select * from logs ";
 		$whereclause = '';
 		$conditions = array();
 		if ($opType != '0' && $opType != 99)
 			$conditions[] = "type = ".$opType;
 		if ($auther != '0') 
 			$conditions[] = "user = '".$auther."'";
 		if ($opcontent != '0')
 			$conditions[] = "record like '%".$opcontent."%'";
 		if ($conditions) 
 			$whereclause = "where ".implode(' and ', $conditions);
 		$sql .= "{$whereclause} order by id desc";
 		if($res = $this->db->findAll($sql))
		{
			return $res;
		}
		else
		{
			return false;			
		} 		
 	}
 	public function getLogIds($offset=0,$number=30,$optype=0, $uid=0, $opcontent='')
 	{
 		$sql = "select * from logs ";
 		$whereclause = '';
 		$conditions = array();
 		if ($optype!=0)
 			$conditions[] = "optype = ".$optype;
 		if ($uid>0) 
 			$conditions[] = "uid = ".$uid;
 		if ($opcontent!='')
 			$conditions[] = "opcontent like '%".$opcontent."%'";
 		if ($conditions) 
 			$whereclause = "where ".implode(' and ', $conditions);
 		$sql .= "{$whereclause} order by id desc limit {$offset}, {$number}";
 		$dbh = $this->dbm->getReadPdo();
 		$res = $dbh->query($sql);
 		$ret = $res->fetchAll();
 		return $ret;
 	}

 	public function getLogNum($optype=0, $user=0, $opcontent='')
 	{
 		$whereclause = '';
 		$conditions = array();
 		if ($optype!=0)
 			$conditions[] = "optype = ".$optype;
 		if ($user>0) 
 			$conditions[] = "user = ".$user;
 		if ($opcontent!='')
 			$conditions[] = "opcontent like '%".$opcontent."%'";
 		if ($conditions) 
 			$whereclause = "where ".implode(' and ', $conditions);
 		
 		$ret['num'] = 0;
 		$sql = "select count(*) as count from logs {$whereclause}";
 		if($res = $this->db->find($sql))
		{
			$num = $res['count'];
			return $num;
		}
		else
		{
			return false;			
		}	
 	}
 	

 	public function fillOperatelog($id,$opuid,$optype,$opcontent,$optime)
 	{
 		$this->id = $id;
 		$this->opuid = $opuid;
 		$this->opcontent = $opcontent;
 		$this->optype = $optype;
 		$this->optime = $optime;
 	}
 	public static function getOperatelog(&$dbm,$opid)
 	{
 		$sql = "select * from operatelogs where id = '{$opid}'";
 		$row = $dbm->getReadPdo()->query($sql)->fetch();
 		$opt = new Operatelog($dbm);
 		$opt->fillOperatelog($row['id'],$row['uid'],$row['optype'],$row['opcontent'],$row['optime']);
 		$opt->fillOpuser();
 		return $opt;
 	}
 
 	public function subText($text, $len=30)
 	{
		$subText = mb_substr($text, 0, $len, 'utf-8');
		$subText  = strlen($subText) < strlen($text)? $subText.'...' : $subText;
		return $subText;
 	}
 
 	//取得所有操作过的用户
 	public function getOpUsers()
 	{
 		global $siteid;
 		$sql ="SELECT DISTINCT uid FROM `operatelogs`";
 		$rows = $this->dbm->getReadPdo()->query($sql)->fetchAll();
 		$profiles = array();
 		foreach ($rows as $i => $row) {
 			if (!$row['uid']) continue;
 			try {
 				$profile = UserProfile::getUserProfile($this->dbm, $row['uid']);
 				
 				$ssouid = $profile->getSSouid();
 				$ssouid_arr = explode('@',$ssouid);
 				
 				$profiles[$ssouid_arr[count($ssouid_arr)-1]][$i] = $profile;
        	}
 				
 			catch (Exception $e) {
 				//操作者为空或找不到，忽略
 			} 	
 			} 		
 		}
 
 }
?>
