<?php
/**
 * class.pagemaker.php
 */

 
/**
 *	实现分页
 */
 class PageMaker
 {
 	private $offset;
 	private $number;
 	private $url;
 	private $flag;
 	private $pre;
 	private $next;
 	private $allnum;
 	private $end;
 	private $allpage;
 	private $prefix;
 	private $shownum;
 
 	
 	/**
 	 *	构造函数，初始化所有类属性
 	 *	@param int $offset 偏移量
 	 *	@param int $number 每页的数量
 	 *	@param string $url 跳转页面地址
 	 *	@param int $allnum 总数量
 	 */
 	public function __construct($offset,$number,$url,$allnum,$offsetname="")
 	{
 		
 		$offset = $offset<0?0:intval($offset);
 		$allnum = $allnum<0?0:$allnum;
 		if($offset>$allnum)
 		{
 			throw new Exception("offset number error!",'21012001');
 		}
 		
 		$this->offset = $offset;
 		$this->number = $number;
 		
 		$this->allnum = $allnum;
 		
 		$this->offsetname = $offsetname;
 		$this->url = $url;
 		$this->pre = $this->offset - $this->number;
 		$this->next = $this->offset + $this->number;
 		$this->end = (ceil($this->allnum/$this->number)-1)*$this->number;
 		$this->flag = $this->number * 5;

 		$this->allpage =  ceil(($this->allnum)/$this->number);
 		
 		
 		
 		if($this->prefix == 'li')
			$this->shownum = 10;
		else
			$this->shownum = 6;	
 	}
 	
 	/**
 	 *	确定是否能翻页
 	 *	@param int $offset 偏移量，用来计算页码
 	 *	@param boolen $href 确定是否能翻页，为TRUE是可翻页，否则不能翻页
 	 *	@return string 当能翻页时，显示一个能翻页的页码，否则显示一个不能翻页的页码
 	 */
 	public function oneLink($offset,$href)
 	{
 		
 		$page =ceil($offset/$this->number)+1;
 		if($href)
 		{
 			if ($offset == 0)
 			{
 				//$ret = str_replace($this->offsetname, "", "<li><a href=".$this->url.">".$page."</a></li>");
 				$ret = str_replace($this->offsetname, 0, "<a href='".$this->url."'><div class=\"pageitem\">".$page."</div></a>");
 			}
 			else
 			{
 				$ret = str_replace("OFFSETVALUE", $offset, "<a href=\"".$this->url."\"><div class=\"pageitem\">".$page."</div></a>");
 			}
 			return $ret;
 		}
 		else
 		{
 			return "<div class=\"selected\">".$page."</div>";
 		}
 	}
 	
 	/**
 	 * 获取页码列表
 	 */
 	public function makePage()
 	{
	
 		if($this->allnum <=0)
			return ""; 
		
		if($this->allnum <=$this->number)	
			return "";
 		$start = $this->offset<$this->flag?0:(floor($this->allnum-($this->offset - $this->flag))/$this->number<10?((floor($this->allnum/$this->number)-9)*$this->number):$this->offset - $this->flag);
 		$start = $start<0?0:$start;
 		$pageStr = '';
 		for($i=0;$i<10;$i++)
 		{
 			if($this->offset==$start)
 				// echo $this->oneLink($start,false);
 				$pageStr .= $this->oneLink($start,false);
 			else
 				// echo $this->oneLink($start,true);
 				$pageStr .= $this->oneLink($start,true);
 			$start += $this->number;
 			if($start>=$this->allnum)
 				break;
 			
 		}

 		return $pageStr;
 		
 	}
 	
 	/**
 	 *	获取上一页内容
 	 *	@param mixed $linkname 当能向上翻页时，作为链接名显示
 	 *	@return 如果当前是第一页（$this->pre<0），则不显示任何内容，否则显示链接到上一页的链接名,即$linkname
 	 */
 	public function getPre($linkname)
 	{
 		if($this->allnum <=0)
			return ""; 
 		if($this->pre<0)
 			return "";
 		else
 			$ret = str_replace("OFFSETVALUE", $this->pre, "<div class=\"pageitem\"><a href='".$this->url."'>".$linkname."</a></div>");
 			return $ret;
 	}
 	
 	/**
 	 *	获取第一页内容
 	 *	@param mixed $linkname 当能翻到第一页时，作为链接名显示
 	 *	@return 如果当前已是第一页，不显示任何内容，否则显示链接到第一页的链接名,即$linkname
 	 */
 	public function getFirst($linkname)
 	{
 		$lim = ($this->allnum/$this->number)>10?5:9;
 		
 		if($this->allnum <=0)
			return ""; 		
 		if($this->pre<0 ||($this->offset/$this->number)<=$lim)
 			return "";
 		else
 		{
 			$ret = str_replace($this->offsetname, 0, "<div class=\"pageitem\"><a href='".$this->url."'>".$linkname."</a></div>");
 			return $ret;
 		}
 	}
 	
 	public function getFirstUrl()
 	{
 		return  str_replace($this->offsetname, "",$this->url);
 	}
 	
 	/**
 	 *	获取下一页内容
 	 *	@param mixed $linkname 当能向上翻页时，作为链接名显示
 	 *	@return 如果当前已是最后一页，不显示任何内容，否则显示链接到下一页的链接名,即$linkname
 	 */
 	public function getNext($linkname)
 	{
 		
 		if($this->allnum <=0)
			return ""; 
 		if($this->next>=$this->allnum)
 			return "";
 		else
 			$ret = str_replace("OFFSETVALUE", $this->next, "<div class=\"pageitem\"><a href='".$this->url."'>".$linkname."</a></div>");
 			return $ret;
 	}
 	
 	/**
 	 *	获取最后一页内容
 	 *	@param mixed $linkname 当能翻到最后一页时，作为链接名显示
 	 *	@return 如果当前已是最后一页，不显示任何内容，否则显示链接到最后一页的链接名,即$linkname
 	 */
 	public function getEnd($linkname='')
 	{
 		$nowpage = floor($this->offset/$this->number) +1;
 		
 		if($this->allnum <=0)
			return ""; 
 		if($this->end==$this->offset || $this->allpage < 11 || $nowpage+5 >$this->allpage)
 			return "";
 		else
 		{
 			if($linkname) {
 				$ret = str_replace("OFFSETVALUE", $this->end, "<div class=\"pageitem\"><a href='".$this->url."'>".$linkname."</a></div>");
 			} else {
 				$ret = str_replace("OFFSETVALUE", $this->end, "<div class=\"pageitem\"><a href='".$this->url."'>...".$this->allpage."</a></div>");
 			}
 			return $ret; 
 		}
 	}
 	
 	public function getAllnum()
 	{
 		return $this->allnum;
 	}
 	public function getOffset()
 	{
 		return $this->offset;
 	}
 	public function makeNav($forumid,$boardid,$topicid,$vforumoffset)
 	{
 		global $pathroot;
 		
 		if($this->allnum<=$this->number)
 			return "";
 		
 		$vf = $vforumoffset==0?"":"/offset-".$vforumoffset;
 		$start = 0;
 		$endpage = floor($this->allnum/$this->number)+1;
 		$nav = "<span class='navlogo'>&nbsp;</span>";
 		for($i=0;$i<6;$i++)
 		{
 			if($start == 0)
 				$nav .="<span><a target='_blank' href='".$pathroot."forum-".$boardid."-".$forumid.$vf."/topic-".$topicid.".html'>".($i+1)."</a></span>";
 			else
 				$nav .="<span><a target='_blank' href='".$pathroot."forum-".$boardid."-".$forumid.$vf."/topic-".$topicid."-".$start.".html'>".($i+1)."</a></span>";
 			
 			$start += $this->number;
 			if($start>$this->allnum)
 				break;
 			
 		}
 		if($start<$this->allnum)
 			$nav .="...<span><a target='_blank' href='".$pathroot."forum-".$boardid."-".$forumid.$vf."/topic-".$topicid."-".$this->end.".html'>".$endpage."</a></span>"; 		
 			$nav .="<span></span>";
 		return $nav;
 		
 	}
 	public function makeFullNav($forumid,$boardid,$topicid,$vforumoffset)
 	{
 		global $pathroot;
 		
 		if($this->allnum<=$this->number)
 			return "";
 		
 		$vf = $vforumoffset==0?"":"/offset-".$vforumoffset;
 		$start = 0;
 		$endpage = floor($this->allnum/$this->number)+1;
 		$nav = "<span><img src='".$pathroot."images/discuz6/navpagelogo.gif'></span>";
 		for($i=0;$i<$endpage;$i++)
 		{
 			if($start == 0)
 				$nav .="<span><a target='_blank' href='".$pathroot."forum-".$boardid."-".$forumid.$vf."/topic-".$topicid.".html'>".($i+1)."</a></span>";
 			else
 				$nav .="<span><a target='_blank' href='".$pathroot."forum-".$boardid."-".$forumid.$vf."/topic-".$topicid."-".$start.".html'>".($i+1)."</a></span>";
 			
 			$start += $this->number;
 			if($start>$this->allnum)
 				break;
 			
 		}
 		if($start<$this->allnum)
 			$nav .="...<span><a target='_blank' href='".$pathroot."forum-".$boardid."-".$forumid.$vf."/topic-".$topicid."-".$this->end.".html'>".$endpage."</a></span>"; 		
 			$nav .="<span></span>";
 		return $nav;
 		
 	}
 	
 	public function getNowpage()
 	{
 		if($this->allnum <=0)
			return ""; 
 			
 		$nowpage = floor($this->offset/$this->number) +1;
 		return "<li><a  ><B>".$nowpage." / ".$this->allpage."</B></a></li>";
 	}
 	public function getGoToPage($fix="")
 	{
 		//return $this->url;
 		if($this->allpage > 1)
 			return "<div class=\"goto\"><input type=\"text\" class='".$this->url."' title='输入页码直接跳转' id=\"gotopage\" maxlength='".$this->number."' value=\"转至\" name='".$this->allpage."' /><div class=\"gobutton\"><a name='gotobtn' href='javascript:;'  onclick='javascript:makeGoto();return false;' id='gotobtn".$fix."'>GO</a></div></div>";
 		else
 			return "";
 	}
 	
 	public function getAllPage()
 	{
 		return $this->allpage;
 	}
 	
 	public function getThisPageStartNumber() 
 	{
 		return $this->offset;
 	}
 	
 	public function getThisPageEndNumber() 
 	{
 		$end = $this->offset + $this->number;
 		$end = $end <= $this->allnum? $end : $this->allnum; 
 		return $end;
 	}
 	
 	public function getGoToPageChinaacc($fix="")
 	{
 		if($this->allpage > 1)
 			return "<div class=\"goto\"><input type=\"text\" class='".$this->url."' title='输入页码直接跳转' id=\"gotopage\" maxlength='".$this->number."' value=\"\" name='".$this->allpage."' /><div class=\"gobutton\"><a name='gotobtn' href=\"javascript:\" onclick=\"javascript:makeGoto();return false;\" id='gotobtn".$fix."'>GO</a></div></div>";
 		else
 			return "";
 	}
 }
 
?>
