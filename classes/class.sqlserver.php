<?php

/**
 * user数据库公共操作类
 * 目前是一个用户名对用一个id
 */
class Sqlserver extends Common {

    private $db;

    function __construct() {
        parent::__construct();
        $this->db = new DbModel('Sqlserver');
    }

    public function userinfo($where) {
        if ($where) {
            $sql = "SELECT a.adminID,b.roleID FROM RAD_ADMIN_USER as a left join RAD_ADMIN_USER_ROLE as b on a.adminID=b.adminID
		WHERE a.adminName = '{$where}' and a.status=1 and a.adminID>0 group by a.adminID,b.roleID";
            if ($res = $this->db->findAll($sql)) {
                return $res;
            }
        }
    }
    
    
    public function roleinfo($condition,$field) {
         if ($condition or $field) {
            $sql = "SELECT {$field} from RAD_ADMIN_ROLE where {$condition}";
            
            if ($res = $this->db->find($sql)) {
                return $res;
            }
        }
    }

    //菜单
    public function getmenu() {
        $role = $_SESSION['user']['ruls'];
        $systemType = getConfig('systemType');
        if ($role && $systemType) {
            $sql = "with tmp as (
SELECT distinct p.privilegeID,p.privilegename,p.actionURL FROM RAD_ADMIN_PRIVILEGE p,RAD_ADMIN_ROLE_PRIVILEGE r 
where r.privilegeID = p.privilegeID and p.treeType> 1 and r.roleID in (
	         " . $role . "
	        ) and p.systemType=" . $systemType . " and p.isRegular = 0
	        union  
	        SELECT distinct p.privilegeID,p.privilegename,p.actionURL 
	        FROM RAD_ADMIN_PRIVILEGE p,RAD_ADMIN_USER_PRIVILEGE a 
	        where a.privilegeID = p.privilegeID and p.treeType > 1 and a.adminID=1 and 
p.systemType=" . $systemType . "  and p.isRegular = 0
) select tmp.actionURL,tmp.privilegename,tmp.privilegeID from tmp 
        union 
        select distinct p.actionURL,p.privilegename,p.privilegeID
        from RAD_ADMIN_PRIVILEGE p,tmp,RAD_ADMIN_PRIVILEGE_REL r 
        where p.privilegeID=r.relPrivilegeID and tmp.privilegeID=r.privilegeID and p.isRegular = 0
        union 
        select distinct p.actionURL,p.privilegename,p.privilegeID
        from RAD_ADMIN_PRIVILEGE p,tmp,RAD_ADMIN_PRIVILEGE_REL r 
        where p.privilegeID=r.relNeighborID and tmp.privilegeID=r.privilegeID and p.isRegular = 0";

            if ($res = $this->db->findAll($sql)) {
                return $res;
            }
        }
    }

}

?>