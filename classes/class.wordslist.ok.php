<?php
	ini_set('display_errors', 'off');
	//error_reporting(E_STRICT);
	class WordsList extends Common{
		private $db;
		// public $tablearr = array();
		private $tables = '';
		private $oldData = array();

	
		function __construct(){
			parent::__construct();
			$this->db = new DbModel('mysql');		    
		}

		function query($sql){
			return $this->db->query($sql);
		}
		//添加数据
		function add($PostData){

			$wordarr = explode('|',$PostData['word']);
			$sql = "insert into {$PostData['table']}(word,version,sen_level,sen_type,sisteid,systype,iswhitelist,create_time,update_time) values";
			foreach ($wordarr as $key => $value) {
				if ($key == 0) {
					$sql .= "('{$value}','{$PostData['version']}',{$PostData['sen_level']},{$PostData['sen_type']},{$PostData['sisteid']},{$PostData['systype']},{$PostData['iswhitelist']},now(),now())";
				}else{
					$sql .= ",('{$value}','{$PostData['version']}',{$PostData['sen_level']},{$PostData['sen_type']},{$PostData['sisteid']},{$PostData['systype']},{$PostData['iswhitelist']},now(),now())";
				}
				
			}
			if ($res = $this->db->execute($sql)) {
				return true;
			}else{
				return false;
			}
		}
		//获取数据库中数据条数
		public function getWordNum($sql){
			//$sql = "select count(id) as num from words where iswhitelist = {$iswhitelist}";
			if($res = $this->db->find($sql)){
				$num = $res['num'];
				return $num;
			}else{
				return false;
			}
		}
		//获取全部的黑名单
		public function getAllBlack(){
			$sql = "select id,word,version,type,sisteid,systype,create_time,update_time from words order by update_time desc";
			return $sql;
			if ($res = $this->db->findAll($sql)) {
				$blackwords = $res;
				return $blackwords;
			}else{
				return false;
			}
		}
		//数字编号转换为字符串
		public function typeToStr($no,$genre){
			$wordsarr = getConfig("$genre");

			foreach ($wordsarr as $key => $value) {
				if (intval($no) == intval($key)) {
					return $value;
				}
			}
			return false;
		}
		
		//分页显示数据
		public function getPageData($where,$offset = '',$limit = '',$iswhitelist = 0){
 			$where .= " and iswhitelist = {$iswhitelist} ";
// 			$where .= $this->joint($parameterArr);
// 			if(!empty($where)){
// 				$w = "where {$where}";
// 			}
			$data_sql = "select id,word,sen_level,version,sen_type,sisteid,systype,create_time,update_time from words where{$where} order by update_time desc";
		
			$num_sql = "select count(id) as num from words where {$where}";
			//echo $data_sql;
			//return $num_sql;
			$resnum = $this->getWordNum($num_sql);
			//return $resnum;
			
			if($limit !=0)
			{
				$data_sql .= " limit $offset,$limit";				
			}	
			
			//secho $sql;
			//$sql .= " order update_time desc";
			//return $res = $this->db->findAll($sql);
			$resdata = $this->db->findAll($data_sql);
			
			$res['data'] = $resdata;
			$res['num'] = $resnum;
			if($res)
			{
				return $res;
			}
			else
			{
				return false;
			}
			
		}
		//拼接where条件
		public function joint($parameterArr = array()){
			if (empty($parameterArr)){
				$where = " and 1 = 1";
			}else {
				if (isset($parameterArr['id'])) {
					$where .= " and id = {$parameterArr['id']}";
				}
				if (isset($parameterArr['word']) && !empty($parameterArr['word'])) {
					# code...
					$where .= " and word like '%{$parameterArr['word']}%'";
				}
				if (isset($parameterArr['version']) && !empty($parameterArr['version'])) {
					# code...
					$where .= " and version like '%{$parameterArr['version']}%'";
				}
				if ($parameterArr['sen_level'] != -1 && isset($parameterArr['sen_level'])) {
					# code...
					$where .= " and sen_level = {$parameterArr['sen_level']}";
				}
				if ($parameterArr['sen_type'] != -1 && isset($parameterArr['sen_type'])) {
					# code...
					$where .= " and sen_type = {$parameterArr['sen_type']}";
				}
				if ($parameterArr['sisteid'] != -1 && isset($parameterArr['sisteid'])) {
					# code...
					$where .= " and sisteid = {$parameterArr['sisteid']}" ;
				}
				if ($parameterArr['systype'] !=-1 && isset($parameterArr['systype'])) {
					# code...
					$where .= " and systype = {$parameterArr['systype']}";
				}
			}
			return $where;
			
		}
		//删除数据
		public function del($wordid){
			$sql = "delete from words where id = $wordid";

			if ($res= $this->db->execute($sql)) {
				# code..
				return true;
			}
			return false;
		}
		//修改数据
		public function edit($table,$editData){
			$sql = "update {$table} set word = '{$editData['word']}',version = '{$editData['version']}',sen_level = '{$editData['sen_level']}',sen_type = {$editData['sen_type']},sisteid = {$editData['sisteid']},systype = {$editData['systype']},update_time = now() where id = {$editData['id']}";
			if ($this->db->execute($sql)) {
				# code..
				return true;
			}else{
				return false;
			}
		}

		/**
		*	批量导入
		*/
		public function batchInput($word_arr = array())
		{

			if(!empty($word_arr))
			{
				foreach($word_arr as $word)
				{
					
					$res = $this->inputOne($word['word'],$word['version'],$word['sen_level'],$word['sen_type'],$word['sisteid'],$word['systype'],$word['iswhitelist']);
					//echo $res;
					// return $res;
					// exit;
					if($res == 1)
					{
						echo $word['word']."&nbsp;&nbsp;".$word['version']."&nbsp;&nbsp;".$word['sen_level']."&nbsp;&nbsp;".$word['sen_type']."&nbsp;&nbsp;".$word['sisteid']."&nbsp;&nbsp;".$word['systype']."&nbsp;&nbsp;".$word['iswhitelist']."---添加成功！<br>";
					}
					elseif($res == 0)
					{
						echo $word['word']."&nbsp;&nbsp;".$word['version']."&nbsp;&nbsp;".$word['sen_level']."&nbsp;&nbsp;".$word['sen_type']."&nbsp;&nbsp;".$word['sisteid']."&nbsp;&nbsp;".$word['systype']."&nbsp;&nbsp;".$word['iswhitelist']."---<font color='red'>添加失败！</font><br>";
					}
					elseif ($res == 2) {
						echo $word['word']."&nbsp;&nbsp;".$word['version']."&nbsp;&nbsp;".$word['sen_level']."&nbsp;&nbsp;".$word['sen_type']."&nbsp;&nbsp;".$word['sisteid']."&nbsp;&nbsp;".$word['systype']."&nbsp;&nbsp;".$word['iswhitelist']."---<font color='red'>添加失败！原因：该数据已添加！</font><br>";
					}
				}
			}
			return false;		
		}


		private function inputOne($wordname="",$version="",$sen_level="",$sen_type="",$sisteid="",$systype="",$iswhitelist)
		{
			// if ($version) {
			// 		# code...
			// 	}
			//return $this->ps_exist($wordname);
			//return $this->ps_exist($wordname,$version,$type,$sisteid,$systype);
			if(($this->ps_exist($wordname)))
			{
				return 2;
			}
			
			$sql = "insert into words (word,version,sen_level,sen_type,sisteid,systype,iswhitelist,create_time,update_time)values('{$wordname}','{$version}','{$sen_level}',$sen_type,$sisteid,$systype,$iswhitelist,now(),now())";
			//return $sql;
			if($this->db->execute($sql))
			{
				// $record = '添加了一条关键词记录：'.$wordname;
				// Operatelog::add($this->db,$record);						
				return 1;
			}	
			return 0;		
		}

		/**
		*	
		*/
		function ps_exist($wordname)
		{
			$sql = "select id from words where word = '{$wordname}'";
			//return $sql;
			// $rs = $this->db->find($sql);
			// return $rs;
			if($rs = $this->db->find($sql))
			{
				return true;
			}
			return false;
		}

		//检验关键词是否唯一
		public function getIdByName($wordname){
			$seaArr = explode('|',$wordname);

		    $seaStr = "'".implode("','",$seaArr)."'";
			$sql = "select word from words where word in ($seaStr)";
			
			$findData = $this->db->findAll($sql);
			foreach ($findData as $key => $value) {
				$result[] = $value["word"];
			}
			$res = array_intersect($seaArr, $result);
			return $res;
		}

	}

?>