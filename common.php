<?php 

/* common.php
* 常用方法
* 
* Created on 2020-9-15
* @author ggkt
* @package base
* @version 1.0.0.0
*/
//require_once dirname(__FILE__)."/config/config.php"
function getConfig($key = null)
{
	static $config = array();
	static $count = 0;
	if(!$config){
		$config = include  dirname(__FILE__)."/config/config.php";
	}

	return isset($config[$key]) ? $config[$key] : $config;
}

?>
