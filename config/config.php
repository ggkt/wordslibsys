<?php
return array(
    //SqlServer
    //测试
     'Sqlserver' => array(
         'DB_HOST' => '192.168.192.256',
          'DB_NAME' => '8888',
          'DB_USER' => 'sa',
          'DB_PASS' => 'password',
          'DB_PORT' => '1433',
          'Freetds' => 'sql2016',
      ),
    //MySQL
    //线上
    'mysql' =>  array(
        'DB_HOST' => '192.168.190.12',
        'DB_NAME' => 'filter_words',
        'DB_USER' => 'root',
        'DB_PASS' => '123456',
        ),
    'sen_type' => array(1 => '涉暴', 2 => '涉政', 3 => '广告法',4 => '涉黄'),
    'sisteid' => array('全网通用','A网'),
    'systype' => array(0 => '通用', 1 => 'bbs', 2 => 'cms'),
    'table' => array(0 => 'words'),
	'sen_level' => array(1 => '一级',2 => '二级'),
    'iswhitelist' => array(0 => '否'),
);
?>

