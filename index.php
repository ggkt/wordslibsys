<?php
header("Content-type: text/html; charset=UTF-8");
require_once dirname(__FILE__) . '/init.php';
require_once dirname(__FILE__) . '/common.php';

$Common = new Common();
#$Common->islogin();
$hour = date('H');
//echo $hour;
if ($hour >= 7 && $hour < 12) {
    $wel_msg = '上午好';
} else if ($hour >= 12 && $hour < 14) {
    $wel_msg = '中午好';
} else if ($hour >= 14 && $hour < 18) {
    $wel_msg = '下午好';
} else {
    $wel_msg = '晚上好';
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <title>正保远程教育敏感词管理系统</title>
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/matrix-login.css" />
    <script src="js/jquery-1.4.2.min.js"></script>  
    <script src="js/common.js"></script>
    <head>
        <link rel="Shortcut Icon" href="images/favicon.gif">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <style>
                .rightmain{height:100%;}
                .zdbtn {
                    float: left;
                    width: 9px;
                    vertical-align: middle;
                    margin-top: 20%;
                    cursor: pointer;
                }
                .nav_logo img{height: 100%;margin-bottom: 10px;max-width: none;}
            </style>

    </head>
    <body>
        <div id="mainDiv">
            <div class="nav_title">
                <span class="nav_logo">词库管理系统</span>
                <span class="top_right"><?php echo $wel_msg . ','; ?><?php echo isset($_SESSION['user']['username']) ? $_SESSION['user']['username'] : ''; ?><span class="logout"><a href="javascript:;" id='logout'>注销</a></span></span>
                <div class="zdbtn"><img src="images/switch_left.jpg" class="switchimg" width="7" height="47" /></div>
            </div>
            <div id="mainDiv">
                <div id="centerDiv">	
                        <div id="left">
                            <div id="lhead">管理菜单</div>
                            <ul>
                                        <li ><a href="blacklist/view/index.php" target="main">黑名单</a></li>	
                                        <li ><a href="whitelists/view/index.php" target="main">白名单</a></li>	
                            </ul>
                        </div>
                    <div id="right"> 
                        <div class="rightmain">			
                            <iframe src="blacklist/view/index.php" id="main" name="main" width="100%" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>


                <script src="js/jquery-1.4.2.min.js"></script>
                <script src="js/common.js"></script>
                <script type="text/javascript">
                    $(function () {
                        $("#centerDiv ul li a").first().css("background-color", "grey");
                    })

                    $("#centerDiv ul li a").click(function () {
                        $("#centerDiv ul li a").each(function () {
                            $(this).css("background-color", "#2E363F");
                        });
                        $(this).css("background-color", "grey");

                    });

                </script>
            </div>
        </div>
    </body>
</html>
