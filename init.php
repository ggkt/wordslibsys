<?php 



include("config/config.inc.php");
function __autoload($class_name)
{
	$filename = 'class.'.strtolower($class_name). '.php';
	if (file_exists(dirname(__FILE__).'/classes/'.$filename))
	{
		require_once dirname(__FILE__).'/classes/'.$filename;
	}

}

// $field_type_arr = array(
// 'INT','VARCHAR','TINYINT','TEXT','DATE','CHAR'
// 	);
?>