var ajaxtimeout = 5000;
$(function(){  
	copyFun();
		//注销
		$("#logout").click(function(){
		var act = 'logout';
		$.ajax
		({
			type: "post",
			data: {"act":act},
			url: "ajaxlist.php",
			dataType:"json",
		//	timeout: ajaxtimeout,
			beforeSend:function()
			{
				
			},
			success: function(text)
			{
				if(text)
				{       
					location.href = text.url;
				}
				
			},
			error: function(){}
		})
	})
	//复制功能
	function copyFun()
	{
		$("input[id^=copy]").each(function(i){  
        //ZeroClipboard.swf和ZeroClipboard.js不在相同目录下需要设置动画文件路径  
        ZeroClipboard.setMoviePath("../js/zeroclipboard/ZeroClipboard.swf");  

             	var clip = new ZeroClipboard.Client();    
	            clip.setHandCursor(true);   
	            var obj = $(this);  
	            //$objvalue = obj.val()
	            //var txt = obj.val() == 'CopyName'?(obj.attr("username")):(obj.attr("passwd"));
	            // var txt = obj.attr("name");
	            var passwdid = (obj).parent("td").parent("tr").find("#id").text();
	           
	            var txt = $(obj).prev("span").text();
	             // console.log(txt);
	            var id = obj.attr("id"); 
	           //设置指向光标为手型   
   				clip.setHandCursor( true );  
           		clip.setText(txt);
           		clip.glue(id); 
           		clip.addEventListener( "complete", function(){    
		                showCopy('复制成功');

             })
    	});  
	}
    

    //分页
    $("#gotopage").keydown(function(e)
	{
		if(e.keyCode == 13)
		{
			makeGoto();
		}
	})
	

    $("#gotopage").click(function()
	{
		if( $("#gotopage").val() == '转至')
			 $("#gotopage").val('');
	});
	$("#gotopage").blur(function()
	{
		if( $("#gotopage").val() == '')
			 $("#gotopage").val('转至');
	});
});  

	function showCopy(msg) {
		$('#copy_content').remove();
		addDiv('copy_content','copy_content');
	//	$("#copy_content").css({top:($(document).scrollTop()+20)+"px"});
		var scTop = $(document).scrollTop();
		$('#copy_content').css({top:0,position:"absolute",opacity: 1,color: "red","margin-left":"400px","text-align":"center","width":"180px","height":"40px","line-height":"40px","background-color":"rgb(71, 41, 41)"});
		$('#copy_content').html(msg);
		$('#copy_content').show();
		$('#copy_content').animate({
			top: scTop+200+"px"
		}, "slow");
		$('#copy_content').animate({
			opacity: 0
		}, 1000);
}

function addDiv(divid,css,before)
{
	if(before == undefined || before == "")
		before = "body";
	$(before).append("<div id='"+divid+"' class='"+css+"' ></div>");	
}


//实现显示密码功能
var num = 0;
var inputId;

function getStr(inputId)
{
	var psaddInput = '<tr>';
	psaddInput += '<td class="need">*</td>';
	psaddInput += '<td>登录帐号：</td>';
	psaddInput += '<td><input type="text" value="" name="psusername[]" class="inputxt" datatype="*" nullmsg="请设置登录帐号！" /></td>';
	psaddInput += '<td><div class="Validform_checktip">登录帐号不能为空</div></td>';
	psaddInput += '</tr>';
	psaddInput += '<tr>';
	psaddInput += '<td class="need">*</td>';
	psaddInput += '<td>登录密码：</td>';
	psaddInput += '<td><input type="password" value="" name="pspasswd[]" class="inputxt" datatype="*" nullmsg="请设置登录密码！" /><img class="showPasswd" id="'+inputId+'" src="../images/eye.png"></td>';
	psaddInput += '<td><div class="Validform_checktip">登录密码不能为空</div></td>';
	psaddInput += '</tr>';

	return psaddInput;
}

// 实现添加域名、项目、项目路径、JBOSS路径
function getDomainStr(inputId,tdname,formname) {
	var serveradd = '<tr id='+inputId+'>';
            serveradd +='<td class="need"></td>';
            serveradd +='<td>'+tdname+'</td>';
            serveradd +='<td><input type="text" value="" name="'+formname+'" class="inputxt" /></td>';          
	    serveradd +='<td><a href="javascript:;" onclick="removeOpt(\''+inputId+'\')">删除</a></td>';
	    serveradd +='</tr>';	    
	    return  serveradd ;
}
function removeOpt(inputId) {
	$("#"+inputId).remove();
}



function addaInput()
{
	
	var inputId = 'showPasswd'+num++;
	$("#aposition").before(getStr(inputId));
	showPwdFun($("#"+inputId));
}
function addDomainInput(trid,tdname,formname) {
	var inputId = 'domain'+num++;
	$("#"+trid).before(getDomainStr(inputId,tdname,formname));
}
function showPwdFun(obj)
{
	var toggleFlag = true;
	var passwdValue;
	var originHtml;
	obj.click(function(){	
		if(toggleFlag)
		{
			passwdValue = obj.prev().val();
			originHtml = obj.prev();

			var showPw = '<input type="text" value="'+passwdValue+'" name="pspasswd[]" class="inputxt" datatype="*" nullmsg="请设置登录密码！" />';
			obj.prev().replaceWith(showPw);
			toggleFlag = false;
		}else{
			obj.prev().replaceWith(originHtml);
			toggleFlag = true;
		}	
	})
}
//改变字段状态
function changeStatus(field_name,status)
{
	var change_status = status == 1?0:1;
	var act="changeStatus";

	$.ajax
	({
		type: "post",
		data: {"act":act,"field_name":field_name,"change_status":change_status},
		url: "../ajaxlist.php",
		dataType:"json",
	//	timeout: ajaxtimeout,
		beforeSend:function()
		{
			
		},
		success: function(text)
		{
			if(text.status=='y')
			{
				window.location.reload(true);
			}else{
				alert(text.info);
			}
			
		},
		error: function(){}
	})
}
function del_confirm()
{
	if(confirm('请慎重操作！删除后该字段下的所有数据都会永久消失！确定要删除吗？'))
	{
		if(confirm('请再次确认！'))
		{
			return true;
		}
	}
	return false;
}

//检测域名不能为空
function checkdomain() {
    var domain = document.getElementById("domain");
    if (domain==null||domain.value=="") {
        return "isnull";
    }
    else{
        return "yes";
    }
}
//检测主题不能为空
function checkdomain() {
    var title = document.getElementById("title");
    if (domain==null||domain.value=="") {
        return "isnull";
    }
    else{
        return "yes";
    }
}
