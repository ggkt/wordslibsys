<?php
session_start();

require_once dirname(__FILE__) . '/init.php';
require_once dirname(__FILE__) . '/common.php';
$sessid = $_SESSION['user']['sessid'];
$oauth_login_url = getConfig('oauth_login_url');
$goto_url = getConfig('goto_url');

$url = $oauth_login_url.'/public/login/isLogout.shtm?goto_url='.$goto_url.'/&sessid='.$sessid;
session_destroy();
header('location:'.$url);
?>
