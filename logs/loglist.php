<?php
	require_once dirname(__FILE__).'/../init.php';
	try{
		$operatelog = new Operatelog();
		$operatelog->islogin();	//没有登录跳转到登录页
		$validate = new Validate();
		$optype = $operatelog->types;
		

		if(isset($_POST['optype']))
		{
			$postType = $validate->filterVar("post",'optype',Validate::$DEFAULT,'0');
			$opcontent = trim($validate->filterVar("post",'opcontent',Validate::$DEFAULT,'0'));
			$auther = trim($validate->filterVar("post",'auther',Validate::$DEFAULT,'0'));
			if($postType != '0' || $opcontent != '0' || $auther != '0')
			{
				$lists = $operatelog->getBy_($postType,$opcontent,$auther);
			}
		}else{
			$order = 'id DESC';
			$limit = 15;	
			$offset = isset($_GET['offset'])?$_GET['offset']:0;
			$lists = $operatelog->logList($order,$limit,$offset);
			$num = $operatelog->getLogNum();
			$pages = new PageMaker($offset,$limit,"loglist.php?offset=OFFSETVALUE", $num, "OFFSETVALUE");
		}
		
	}catch(Exception $e)
	{
		echo '<center><h1><font color="red">程序出错了，请查看日志！</font></h1></center>';
		Debug::writeLogs($e->getMessage()) ;
	}

?>
<link rel="stylesheet" href="../css/common.css" />

<h2>操作记录</h2>
<table class="tablelist">
	<tr>
		<td>
			<form method="post" action="" id="myform">
			<select name="optype">
				<option value="0">选择类型</option>
				<?php foreach($optype as $k=>$v):?>
					<option value="<?=$k?>" <?php if(isset($postType) && $k == $postType) echo "selected = selected";?>><?=$v?></option>
				<?php endforeach;?>
			</select>&nbsp;&nbsp;&nbsp;
			内容：<input type="text" name="opcontent" id="opcontent" value="<?php if(isset($opcontent) && $opcontent != '0') echo $opcontent; ?>" />&nbsp;&nbsp;&nbsp;
			操作人：<input type="text" name="auther" id="auther" value="<?php if(isset($auther) && $auther != '0') echo $auther; ?>"/>&nbsp;&nbsp;&nbsp;
			<input type="submit" id= "search" value="搜索" >
			</form>
		</td>
	</tr>
</table>

<table class="tablelist">
<tr>
<td>
	<?php if(isset($pages)): ?>
	<span class="pages">[ 共 <?php echo $pages->getAllPage();?> 页,<?=$num?>条记录]</span>  
	<?php endif;?>
</td>
</tr>
</table>

<table class="tablelist">
<tr>
<th>编号</th>
<th>操作人</th>
<th>内容</th>
<th>IP</th>
<th>时间</th>
</tr>
<?php if(!empty($lists)):?>
<?php foreach ($lists as $log): ?>
<tr>
<td><?=$log['id'];?></td>
<td><?=$log['user'];?></td>
<td><?=$log['record'];?></td>
<td><?=$log['ip'];?></td>
<td><?=$log['time'];?></td>
</tr>
<?php endforeach; ?>

<?php else : ?>
<tr>
	<?php if(isset($_POST['optype'])): ?>
		<h1 style='text-align:center;color:red;'>搜索记录为空</h1>
	<?php else: ?>
		<h1 style='text-align:center;color:red;'>当前没有操作记录！</h1>
	<?php endif; ?>

</tr>
<?php endif;?>
</table>

<?php if(isset($pages)): ?>
<div class="showpage">
			
	<div><?php echo $pages->getFirst('首页'); ?></div>
	<div><?php echo $pages->getPre('上一页'); ?></div>
	<div><?php echo $pages->makePage(); ?></div>
	<div><?php echo $pages->getNext('下一页'); ?></div>
	<div><?php echo $pages->getEnd('尾页');?></div>
	<!--<span class='goto'>转至<input type='text'  title='输入页码直接跳转' id='gotopage' maxlength='6' value='1' name='gotopage' /><a name='gotobtn1' href='javascript:'' onclick='maketo();return false;'' id='gotobtn'>&nbsp;&nbsp;GO</a></span>-->
	<?php echo $pages->getGoToPage(); ?>
</div>
<?php endif; ?>
<script src="../js/jquery-1.4.2.min.js"></script> 
<script src="../js/common.js"></script> 
<script type="text/javascript">
function makeGoto()
{
	offset = $("#gotopage").val();
	if(offset != "" &&　offset>0 && offset <= <?php echo $pages->getAllPage();?>)
	{
		url = "loglist.php?offset="+15*(offset-1);
		location.href = url;
	}
	else
	{
		alert('请输入正确的页码');
	}
	
}
</script>