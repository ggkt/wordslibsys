<?php
try{
	if ($wordid = $_GET['id']) {
		# code...
		require_once dirname(__FILE__)."/../../init.php";

		$wordslist = new WordsList();
		$validate = new Validate();

		$wordid = $validate->filterVar('get','id',Validate::$DEFAULT,'');

		if($wordslist->del($wordid)){
// 			echo '<div style="text-align:center;">删除成功！<a href="../view/index.php">返回列表</a></div>';
// 			header('refresh:1;url=../view/index.php');
			echo "<script>alert('删除成功');</script>";
			echo "<script>window.location.href='../view/index.php';</script>";
		}else{
			echo "<script>alert('删除失败');</script>";
			echo "<script>window.location.href='../view/index.php';</script>";
		}
	}else{
		echo "id 获取异常！！！";
	}
	
}catch(Exception $e){
	echo "<center><h1><font color='red'>程序出错了，请查看日志！</font></h1></center>";
	Debug::writeLogs($e->getMessage());
	
}

?>