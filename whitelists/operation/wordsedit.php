<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="stylesheet" href="../../css/common.css" />
    <script src="../../js/jquery-1.4.2.min.js"></script> 
	<script src="../../js/Validform_v5.3.2.js"> </script>
    <script type="text/javascript">
    $(function(){
		wordname = $("input[name='wordname']").attr("value");
		//alert(wordname);
		get_wordname = "<?php echo $_GET['wordname']; ?>";

		if(wordname == get_wordname)
		{
			$("input[name='wordname']").removeAttr("ajaxurl");	//名字不变则不验证是否重复
		}

		$("input[name='wordname']").change(function(){
				wordname = $("input[name='wordname']").attr("value");
				//alert($("input[name='username']").attr("value"));
				if(wordname == get_wordname)
				{
					$("input[name='wordname']").removeAttr("ajaxurl");
					//alert($("input[name='username']").attr("ajaxurl"));
				}
				else
				{
					$("input[name='wordname']").attr("ajaxurl","../../ajaxlist.php?act=checkWord");
				}
			});

        $(".userAddForm").Validform({
		  tiptype:2
	   	});
    });
    </script>
</head>
<?php 
require_once dirname(__FILE__).'/../../init.php';
try{	
	$wordslist = new WordsList();
	$validate = new Validate();

	$parameterid = $_GET['id'];
	$parameterArr['id'] = $parameterid;
	$parameterArr['iswhitelist'] = 1;
	//echo 111;
	$res = $wordslist->getPageData($parameterArr);
// 	print_r($oldData);
	$oldData = $res['data'];
// 	exit;
	

	if ($id = $_POST['id']) {
		# code...
		$table = getConfig('table');
		$editData['id'] = $validate->filterVar("post",'id',Validate::$DEFAULT,'');
		$editData['word'] = trim($validate->filterVar("post",'wordname',Validate::$DEFAULT,''));
		$editData['version'] = trim($validate->filterVar("post",'version',Validate::$DEFAULT,''));
		$editData['sen_level'] = $validate->filterVar("post",'sen_level',Validate::$DEFAULT,'');
		$editData['sen_type'] = $validate->filterVar("post",'sen_type',Validate::$DEFAULT,'');
		$editData['sisteid'] = $validate->filterVar("post",'sisteid',Validate::$DEFAULT,'');
		$editData['systype'] = $validate->filterVar("post",'systype',Validate::$DEFAULT,'');

		if ($res = $wordslist->edit($table['0'],$editData)) {
			echo "<script>alert('更新成功');</script>";
			echo "<script>window.location.href='../view/index.php';</script>";
		}else{
			echo "<center><h1><font color='red'>更新失败</font></h1></center>";
		}
	}
	
}catch(Exception $e)
{
	echo '<center><h1><font color="red">程序出错了，请查看日志！</font></h1></center>';
	Debug::writeLogs($e->getMessage()) ;
}
?>

<div class = "wordsedit" >
	<form class="userAddForm" method = "post" action="wordsedit.php">
            <table width="100%" style="table-layout:fixed;">
                 <tr>
                    <td class="need" style="width:10px;">*</td>
                    <td style="width:100px;">关键词：</td>
                    <td style="width:205px;">
					<input type="hidden" name="id" value="<?=$_GET['id']?>">
					<input type="text" value="<?php echo $oldData[0]['word'];?>" name="wordname" class="inputxt" datatype="*" ajaxurl="/../../ajaxlist.php?act=checkWord" errormsg="关键词不能为空" />
					</td>
					<td><div class="Validform_checktip">关键词不能为空</div></td>
                </tr>
                <tr>
                    <td class="need" style="width:10px;">*</td>
                    <td style="width:100px;">版本号：</td>
                    <td style="width:205px;">
					 <input type = "text" value = "<?php echo $oldData[0]['version'];?>" name = "version" class = "inputxt" datatype = "*" nullmsg = "请填写版本号" ajaxurl = "../../ajaxlist.php?act=checkVersion" /></td>
                    <td><div class="Validform_checktip">版本号不能为空</div>
					</td>
                </tr> 
                <tr>
                    <td class="need" style="width:10px;">*</td>
                    <td style="width:100px;">级别：</td>
                    <td style="width:205px;">
					<select name = 'sen_level' id = 'sen_level' class = 'select'>
						<?php $typearr = getConfig('sen_level');
						foreach($typearr as $key => $value):?>
						<option <?php if($key == $oldData[0]['sen_level']){echo "selected = selected";}?> value="<?php echo $key;?>"><?php echo $value;?></option>
						<?php endforeach;?>
					</select>
					</td>
                </tr>
                <tr>
                    <td class="need" style="width:10px;">*</td>
                    <td style="width:100px;">类型：</td>
                    <td style="width:205px;">
					<select name = 'sen_type' id = 'sen_type' class = 'select'>
						<?php $typearr = getConfig('sen_type');
						foreach($typearr as $key => $value):?>
						<option <?php if($key == $oldData[0]['sen_type']){echo "selected = selected";}?> value="<?php echo $key;?>"><?php echo $value;?></option>
						<?php endforeach;?>
					</select>
					</td>
                </tr>
                 <tr>
                    <td class="need" style="width:10px;">*</td>
                    <td style="width:100px;">网站：</td>
                    <td style="width:205px;">
					<select name = 'sisteid' id = 'sisteid' class = 'select'>
						<?php $typearr = getConfig('sisteid');
						foreach($typearr as $key => $value):?>
						<option <?php if($key == $oldData[0]['sisteid']){echo "selected = selected";}?> value="<?php echo $key;?>"><?php echo $value;?></option>
						<?php endforeach;?>
					</select>
					</td>
                </tr>        
                 <tr>
                    <td class="need" style="width:10px;">*</td>
                    <td style="width:100px;">系统：</td>
                    <td style="width:205px;">
					<select name = 'systype' id = 'systype' class = 'select'>
<!-- 						<option value = "-1">不限</option> -->
						<?php $typearr = getConfig('systype');
						foreach($typearr as $key => $value):?>
						<option <?php if($key == $oldData[0]['systype']){echo "selected = selected";}?> value="<?php echo $key;?>"><?php echo $value;?></option>
						<?php endforeach;?>
					</select>
					</td>
                </tr>                       
                <tr>
                    <td class="need"></td>
                    <td></td>
                    <td colspan="2" style="padding:10px 0 18px 0;">
                        <input type="submit" value="提 交" /> <input type="reset" value="重 置" />
                    </td>
                </tr>
            </table>
 	</form>
 </div>

 </html>