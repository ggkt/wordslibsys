<?php session_start();?>
<link rel="stylesheet" href="../../css/common.css" />
<head>
      <meta content="text/html;charset=utf-8" http-equiv="content-type">
</head>
<?php
	
	try{

		
		require_once dirname(__FILE__).'/../../init.php';
		
		$num = 0;
		
		$wordslist = new wordslist();
		$validate = new validate();
		$table = 'words';
		$iswhitelist = 1;
		
		$limit = 10;
		$offset = $validate->filterVar("get",'offset',Validate::$DEFAULT,0);
		$act = $_REQUEST['act'];

		
		$wordname = trim($validate->filterVar("get","wordname",Validate::$DEFAULT,''));
		$search_version = trim($validate->filterVar("get", "version",Validate::$DEFAULT,''));
		$search_sen_type = trim($validate->filterVar("get",'sen_type',Validate::$DEFAULT,''));
		$search_sen_level = trim($validate->filterVar("get",'sen_level',Validate::$DEFAULT,''));
			
		$search_sisteid = trim($validate->filterVar("get",'sisteid',Validate::$DEFAULT,''));
		$search_systype = trim($validate->filterVar("get",'systype',Validate::$DEFAULT,''));
		
		if($act == 's')
		{
			
			
			
			$parameterArr['word'] = $wordname;
			$parameterArr['version'] = $search_version;
			$parameterArr['sen_type'] = $search_sen_type;
			$parameterArr['sen_level'] = $search_sen_level;
			$parameterArr['sisteid'] = $search_sisteid;
			$parameterArr['systype'] = $search_systype;
			$parameterArr['iswhitelist'] = $iswhitelist;
			
		    $res = $wordslist->getPageData($parameterArr,$offset,$limit,$iswhitelist);
	
		    $blackwords = $res['data'];
		    
		    //var_dump($blackwords);
			$num = $res['num'];
			$str = $res['str'];
			$pages = new PageMaker($offset,$limit,"index.php?offset=OFFSETVALUE&act=s&{$str}", $num, "OFFSETVALUE");
			
		}
		elseif($act == "page" || $act == ""){
			
			$res = $wordslist->getPageData($parameterArr,$offset,$limit,$iswhitelist);
			 $blackwords = $res['data'];
			$num = $res['num'];
			$pages = new PageMaker($offset,$limit,"index.php?offset=OFFSETVALUE&act=page", $num, "OFFSETVALUE");
		}

	}catch(Exception $e){
		echo "";
	}
	

?>
 <style type="text/css">
    #bg{ display: none; position: absolute; top: 0%; left: 0%; width: 100%; height: 100%; background-color: #333; z-index:1001; -moz-opacity: 0.7; opacity:.70; filter: alpha(opacity=70);}
    #edit , #edit2 {display: none; position: absolute; top: 25%; left: 22%; width: 53%; height: 49%; padding: 8px; border: 8px solid #E8E9F7; background-color: white; z-index:1002; overflow: auto;}
</style>
<h2>黑名单</h2>
<table class="tablelist">	
	<tr>
		<td>
			<form method="get" action="" id="myform">
			<input type='hidden' name='act' value="s">
			关键字：<input type="text" name="wordname" id="wordname" value="<?php if (!empty($wordname)) echo $wordname;?>" />&nbsp;&nbsp;&nbsp;
			版本号：<input type="text" name="version" id="version" value="<?php if(!empty($search_version)) echo $search_version;?>" />&nbsp;&nbsp;&nbsp;
			
			级别：
				<select name = 'sen_level' id = 'sen_level'>
				<option value="-1">不限</option>
					<?php
						$typearr = getConfig('sen_level'); 
						foreach ($typearr as $key => $value): ?>
						<option <?php if($key == $search_sen_level) {echo "selected = selected";}?> value = "<?php echo $key;?>"><?=$value;?></option>
					<?php endforeach;?>
				</select>
			&nbsp;&nbsp;&nbsp;
			类型：
				<select name = 'sen_type' id = 'sen_type'>
				<option value="-1">不限</option>
					<?php
						$typearr = getConfig('sen_type'); 
						foreach ($typearr as $key => $value): ?>
						<option <?php if($key == $search_sen_type) {echo "selected = selected";}?> value = "<?php echo $key;?>"><?=$value;?></option>
					<?php endforeach;?>
				</select>
			&nbsp;&nbsp;&nbsp;<br /><br />
			网站：
				<select name = 'sisteid' id = 'sisteid'>
				<option value = '-1'>不限</option>
				<?php 
					$sisteidarr = getConfig('sisteid');foreach($sisteidarr as $key=>$value):?>
				<option <?php if ($key == $search_sisteid) {
					echo "selected = selected";
				}?> value="<?php echo $key;?>"><?php echo $value;?></option>
				<?php endforeach;?>
				</select>
			&nbsp;&nbsp;&nbsp;
			系统：
				<select name = 'systype' id = 'systype'>
					<option value = '-1'>不限</option>
					<?php $systypearr = getConfig('systype');foreach($systypearr as $key => $value):?>
					<option <?php if($key == $search_systype){echo "selected = selected";}?> value="<?php echo $key;?>"><?php echo $value;?></option>
				<?php endforeach;?>
				</select>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" id="search" name = "dosubmit" value="搜索" >
			</form>
		</td>
	</tr>
</table>
<table class="tablelist">
<tr>
<td>
	<?php if(isset($pages)): ?>
	<span class="pages">[ 共 <?php echo $pages->getAllPage();?>  页,<?=$num?> 条记录]</span>  
	<?php endif;?>
	<span class = "spancheckbox"  style = "display:none">
		<input type="checkbox" id ="checkAll" onclick = "init()">全选/反选
		<input type="button" onclick = "delPslist()" value = "删除">
	</span>
	<?php if ($_SESSION["user"]["rolename"] == "组长及部门"):?>
	<a href="../operation/wordsadd.php">录入关键词</a> | <a href ="../operation/batchInput.php">批量导入</a> | <a href ="javascript:;" id = "">批量导出</a>
	<?php endif;?>
</td>
</tr>
</table>

<table class="tablelist" >
<tr>
<th>关键词</th>
<th>版本号</th>
<th>级别</th>
<th>类型</th>
<th>网站</th>
<th>系统</th>
<th>添加时间</th>
<th>修改时间</th>
<?php if ($_SESSION["user"]["rolename"] == "组长及部门"):?>
<th>操作</th>
<?php endif;?>
</tr>
<?php if(!empty($blackwords)):?>

<?php foreach ($blackwords as $words):
	// $_SESSION['words'.$words['id']] = $words;
	// $propath1 = addcslashes($propath,'/');
?>
<tr>
	<td style="text-align: center">
		<span class="spancheckbox" style="display: none"><input type='checkbox' class='check' id='delid' value='<?php echo $words['id'];?>'></span>
		<?php echo isset($words['word'])?$words['word']:'无';?>
	</td>
	<td style="text-align: center">	<pre><?php if(isset($words['version'])): echo $words['version'];?><?php endif ?></pre>
	</td>
	<td style="text-align: center">
			<?php 
				echo isset($words['sen_level'])? $wordslist->typeToStr($words['sen_level'],'sen_level'):'无';
			?>
	</td>
	<td style="text-align: center">
			<?php 
				echo isset($words['sen_type'])? $wordslist->typeToStr($words['sen_type'],'sen_type'):'无';
			?>
	</td>
	<td style="text-align: center">
		<pre><?php echo isset($words['sisteid'])? $wordslist->typeToStr($words['sisteid'],'sisteid'):'无';?></pre>
	</td>
	<td style="text-align: center">
		<pre><?php echo isset($words['systype'])? $wordslist->typeToStr($words['systype'],'systype'):'无';?></pre>
	</td>
	<td style="text-align: center">
		<pre><?php echo isset($words['create_time'])? $words['create_time']:'无';?></pre>
	</td>
	<td style="text-align: center">
		<pre><?php echo isset($words['update_time'])? $words['update_time']:'无';?></pre>
	</td>
	<?php if ($_SESSION["user"]["rolename"] == "组长及部门"):?>
	<td>
		<a href="../operation/wordsedit.php?id=<?=$words['id']?>&wordname=<?=$words['word']?>">编辑</a> | 
		<a id = "delwords" onclick="return confirm('确定要删除吗？')" href="../operation/wordsdel.php?id=<?=$words['id']?>">删除</a>
	</td>
	<?php endif;?>
</tr>
<?php endforeach; ?>

<?php else : ?>
<tr>
<?php if(isset($_POST['blackwords'])): ?>
		<h1 style='text-align:center;color:red;'>搜索记录为空</h1>
		<?php else: ?>
		<h1 style='text-align:center;color:red;'>当前没有记录，请添加！</h1>
		<?php endif; ?>
</tr>
<?php endif;?>
</table>
<?php if(isset($pages)): ?>
<div class="showpage">				
	<div><?php echo $pages->getFirst('首页'); ?></div>
	<div><?php echo $pages->getPre('上一页'); ?></div>
	<div><?php echo $pages->makePage(); ?></div>
	<div><?php echo $pages->getNext('下一页'); ?></div>
	<div><?php echo $pages->getEnd('尾页');?></div>
	<?php echo $pages->getGoToPage(); ?>
</div>
<?php endif; ?>

<script src="../../js/jquery-1.4.2.min.js"></script>
<script src="../../js/privilege.js"></script>
<!-- <script src="../js/passwdlist.js"></script> -->
<script type="text/javascript" src="../../js/zeroclipboard/ZeroClipboard.js"></script> 
<script src="../../js/common.js"></script>
<script type="text/javascript">
<?php if(isset($pages)):?>
function makeGoto()
{
	offset = $("#gotopage").val();
	//alert(offset);
	if(offset != "" &&　offset>0 && offset <= <?php echo $pages->getAllPage();?>)
	{
		url = "index.php?offset="+10*(offset-1)+"&act=<?php echo $act;?>"+"&<?php echo $str;?>";
		
		location.href = url;
	}
	else
	{
		alert('请输入正确的页码');
	}
	
}
<?php endif;?>

function both(){
    var checkboxs=document.getElementsByName("checkarr");
    
    for (var i=0;i<checkboxs.length;i++) {
     var e=checkboxs[i];
     e.checked=!e.checked;
    }
}

function bothdel(){
    var checkboxs=document.getElementsByName("checkarr");
    var str =' ';
    for (var i=0;i<checkboxs.length;i++) {
        if(checkboxs[i].checked==true){
            str+=checkboxs[i].value+',';
          
        }
    }
    
    var hint = "确定删除所选记录!";
    if(confirm(hint)){
        if(str===' '){
            alert("请选择需要删除项！");
        }else{
            $.post('batchdel.php',{idarr:str},function(data){
                if(data==true){
                    window.location.reload();
                }else{
                    alert(data);
                }
            });
        }
    }
}
</script>
