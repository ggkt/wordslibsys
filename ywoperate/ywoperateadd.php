<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<link rel="stylesheet" href="../css/common.css" />
<script src="../js/jquery-1.4.2.min.js"></script> 
<script src="../js/Validform_v5.3.2.js"> </script>
<script src="../js/common.js"> </script>
<script type="text/javascript">
function checkform() {
    if (checkform()=="isnull") {
        $('#divoperate').html("请输入内容");
        return false;
    }
    return true;
}
</script>
    </head>
<?php
require_once '../init.php';
try{
    $ywoperate = new Ywoperate();
    $ywoperate->islogin();
    $validate = new Validate();
    if(isset($_POST['title'])){
        if(isset($_POST['title']) && isset($_POST['contents']) && isset($_POST['name'])){
            $operatetitle = $validate->filterVar("post",'title',Validate::$DEFAULT);
            $operatecontents = $validate->filterVar("post",'contents',Validate::$DEFAULT);
          $operateflag = $validate->filterVar("post",'flag',Validate::$DEFAULT);
            $operatename = $validate->filterVar("post",'name',Validate::$DEFAULT);
            if($ywoperate->add($operatetitle,$operatecontents,$operateflag,$operatename)){
               // echo "<script type='text/javascript'>alert('更新成功');</script>";
		header("refresh:0;url=ywoperatelist.php");
            }
            else{
                echo "<script type='text/javascript'>alert('更新失败');</script>";
            }
        }
        }
}catch(Exception $e){
    echo '<center><h1><font color="red">程序出错了，请查看日志！</font></h1></center>';
    Debug::writeLogs($e->getMessage()) ;
}


?>
    <body>
<div class = "useradd">
	<form class="userAddForm" onsubmit="return checkform()" method = "post" action="ywoperateadd.php">
            <table width="100%" style="table-layout:fixed;">
                <tr>
                    <td class="need"></td>
                    <td>是否关注：</td>
                    <td>
                        <input type="radio" name="flag" value="0"> 否
	                    <input type="radio" name="flag" value="1" checked="checked"> 是
                    </td>                  
                </tr>
                <tr>
                    <td  class="need" style="width:10px;">*</td>
                    <td style="width:100px;">标题：</td>
                    <td style="width:200px;">
						<div class="selectfr" style="width:200px;">
						<input  type="text" id="title" name="title" class="name" style="width:200px;"value="" nullmsg="请输入操作标题">
						</div>
						<td><div id="divoperate"></div></td>
                    </td>
                </tr>
                <tr>
                    <td  class="need" style="width:10px;"></td>
                    <td style="width:100px;">操作人：</td>
                    <td style="width:200px;">
						<div class="selectfr" style="width:200px;">
						<input  type="text" id="name" name="name" class="name" style="width:200px;"value="" nullmsg="请输入操作人姓名">
						</div>
                    </td>
                </tr>
                <tr>
                    <td class="need"></td>
                    <td>内容：</td>
                    <td><textarea  name="contents" id= "contents" rows="5" ></textarea></td>                  
                    <td><div class="Validform_checktip"></div></td>
                </tr>
                <tr>
                    <td class="need"></td>
                    <td></td>
                    <td colspan="2" style="padding:10px 0 18px 0;">
                        <input type="submit" value="提 交" /> <input type="reset" value="重 置" />
                    </td>
                </tr>
            </table>
 	</form>
</div>
</body>
</html>
