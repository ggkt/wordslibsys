<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
<link rel="stylesheet" href="../css/common.css" />
<script src="../js/jquery-1.4.2.min.js"></script> 
<script src="../js/Validform_v5.3.2.js"> </script>
<script src="../js/common.js"> </script>
<script type="text/javascript">
function checkform() {
    if (checktitle()=="isnull") {
        $('#divtitle').html("请输入标题");
        return false;
    }
    return true;
}
</script>
	</head>
<?php 
require_once '../init.php';	
try
{
	$ywoperate = new Ywoperate();
	$ywoperate->islogin();	//没有登录跳转到登录页
	$validate = new Validate();
	if(isset($_POST['title']))
	{	
		if(isset($_POST['title']) &&isset($_POST['contents'])&&isset($_POST['flag']))
		{
			$id = $validate->filterVar("post",'id',Validate::$DEFAULT,'');
			$title = $validate->filterVar("post",'title',Validate::$DEFAULT,'');
			$contents = $validate->filterVar("post",'contents',Validate::$DEFAULT,'');
			$flag = $validate->filterVar("post",'flag',Validate::$DEFAULT,'');
			echo $flag;
			if($ywoperate->edit($_POST['id'],$title,$contents,$flag))
			{
				header("refresh:0;url=ywoperatelist.php");
			}else{
			echo "<script type='text/javascript'>alert('数据没有改动');</script>";
				header("refresh:0;url=ywoperatelist.php");
			}
		}
		else
		{
			echo "<script type='text/javascript'>alert('参数有错误');</script>";
		}
	}
}catch(Exception $e)
{
    echo '<center><h1><font color="red">程序出错了，请查看日志！</font></h1></center>';
    Debug::writeLogs($e->getMessage()) ;
}

?>
	<body>
<div class = "useradd" >
	<form class="userAddForm" onsubmit="return checkform()" method = "post" action="ywoperateedit.php">
            <table width="100%" style="table-layout:fixed;">
                 <tr>
                    <td class="need"></td>
                    <td>是否关注：</td>
                    <?php if (isset($_GET['id'])): ?>
                    <td>
                        <input type="radio" name="flag" id="attention" value="1" <?php if ($_SESSION['operate'.$_GET['id']]['flag']=="1") echo 'checked="checked"';?>/> 已关注
	                    <input type="radio" name="flag" id="drop" value="0" <?php if ($_SESSION['operate'.$_GET['id']]['flag']=="0") echo 'checked="checked"';?>/> 未关注
                    </td>                  
                    <?php else: ?>
                    <td>
                        <input type="radio" name="flag" value="0"> 否
	                    <input type="radio" name="flag" value="1" checked="checked"> 是
                    </td>     
                    <?php endif; ?>
                </tr>
                <tr>
                    <td class="need" style="width:10px;">*</td>
                    <input type="hidden" name="id" value="<?php echo isset($_GET['id'])?$_GET['id']:'';?>"/>
                    <td style="width:100px;">标题：</td>
                    <td style="width:205px;">
		    		<input type="text" id="title" value="<?php echo isset($_GET['id'] )?$_SESSION['operate'.$_GET['id']]['title']:'';?>" name="title" class="inputxt" datatype="*" nullmsg="请设置标题！" /></td>
                    <td><div id="divtitle"></div></td>
                </tr>
		<tr>
                    <td class="need"></td>
                    <td>操作人：</td>
                    <td >
		    		<?php echo isset($_GET['id'])?$_SESSION['operate'.$_GET['id']]['name']:'';?>
		    		</td>
                </tr>
		<tr>
                    <td class="need"></td>
                    <td >内容：</td>
                    <td >
		    <textarea rows = "2" name="contents" class="inputxt"><?php echo isset($_GET['id'])?$_SESSION['operate'.$_GET['id']]['contents']:'';?></textarea>
		    </td>
                </tr>
                <tr><td></td><td></td>
                <td colspan="2" style="padding:10px 0 18px 0;">
                        <input type="submit" name="submit" value="提 交" /> <input type="reset" value="重 置" />
                </td>
                </tr>
            </table>
 	</form>
 </div>  
</body>
</html>