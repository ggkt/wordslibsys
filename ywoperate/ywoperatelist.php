<link rel="stylesheet" href="../css/common.css" />
<?php
try{
	require_once dirname(__FILE__).'/../init.php';
	$ywoperate= new Ywoperate();
	$ywoperate->islogin();	//没有登录跳转到登录页
	$user = new User();
	$validate = new Validate();
    $title = trim($validate->filterVar("post",'stitle',Validate::$DEFAULT,''));
	$name = trim($validate->filterVar("post",'sname',Validate::$DEFAULT,''));
	if(isset($_POST['stitle']))	//搜索
	{ 
		if($name != '' || $title != '' )
		{      
			$operatelist = $ywoperate->getBy_($title,$name);
                }
	}else{
		$order = 'id DESC';
		$limit = 10;	
		$offset = isset($_GET['offset'])?$_GET['offset']:0;
		$operatelist = $ywoperate->operatelist($order,$limit,$offset);
		$num = $ywoperate->getOperateNum();
		$pages = new PageMaker($offset,$limit,"ywoperatelist.php?offset=OFFSETVALUE", $num, "OFFSETVALUE");
	}
	
}catch(Exception $e)
{
	echo '<center><h1><font color="red">程序出错了，请查看日志！</font></h1></center>';
	Debug::writeLogs($e->getMessage()) ;
}
	
?>
<h2>运维操作记录</h2>
<table class="tablelist">	
	<tr>
		<td>
			<form method="post" action="" id="myform">
			标题：<input type="text" name="stitle" id="title" value="" />&nbsp;&nbsp;&nbsp;
			操作人：<input type="text" name="sname" id="name" value="" />&nbsp;&nbsp;&nbsp;
			<input type="submit" id="search" value="搜索" >
			</form>
		</td>
	</tr>
</table>
<table class="tablelist">
<tr>
<td>
	<?php if(isset($pages)): ?>
			<span class="pages">[ 共 <?php echo $pages->getAllPage();?> 页,<?=$num?>条记录]</span>  
	<?php endif;?>
	<a href="ywoperateadd.php">添加操作记录</a>
</td>
</tr>
</table>

<table class="tablelist" >
<tr>
<th>主题</th>
<th>操作内容</th>
<th>操作人</th>
<th>添加时间</th>
<th>操作</th>
</tr>
<?php if(!empty($operatelist)):?>

<?php foreach ($operatelist as $operate):
	$_SESSION['operate'.$operate['id']] = $operate;
?>
<tr>
	<td width="20%">	<?php if(isset($operate['title'])): echo $operate['title'];?><?php endif ?>
	</td>
    <td width="50%">
		<?php echo isset($operate['contents'])? $operate['contents']:'无';?>
	</td>
	<td width="10%">
		<pre><?php echo isset($operate['name'])?$operate['name']:'无';?></pre>
	</td>
	<td width="10%">
		<pre><?php echo isset($operate['optime'])?$operate['optime']:'无';?></pre>
	</td>
	<td width="10%">
		<a href="ywoperateedit.php?id=<?=$operate['id']?>">编辑</a>|
		<a id = "deloperate" onclick="return confirm('确定要删除吗？')" href="ywoperatedel.php?id=<?=$operate['id']?>">删除</a> 
	</td>
</tr>
<?php endforeach; ?>

<?php else : ?>
<tr>
<?php if(isset($_POST['title'])): ?>
		<h1 style='text-align:center;color:red;'>搜索记录为空</h1>
		<?php else: ?>
		<h1 style='text-align:center;color:red;'>当前没有记录，请添加！</h1>
		<?php endif; ?>
</tr>
<?php endif;?>
</table>
<?php if(isset($pages)): ?>
	<div class="showpage">				
		<div><?php echo $pages->getFirst('首页'); ?></div>
		<div><?php echo $pages->getPre('上一页'); ?></div>
		<div><?php echo $pages->makePage(); ?></div>
		<div><?php echo $pages->getNext('下一页'); ?></div>
		<div><?php echo $pages->getEnd('尾页');?></div>
		<?php echo $pages->getGoToPage(); ?>
	</div>
<?php endif; ?>
<script src="../js/jquery-1.4.2.min.js"></script> 
<script src="../js/common.js"></script>
<script type="text/javascript">
$(function(){
	$("#delserver").click(function(){
		confirm('确定要删除吗？');
	});
})
function makeGoto()
{
	offset = $("#gotopage").val();
	if(offset != "" &&　offset>0 && offset <= <?php echo $pages->getAllPage();?>)
	{
		url = "ywoperatelist.php?offset="+10*(offset-1);
		location.href = url;
	}
	else
	{
		alert('请输入正确的页码');
	}
	
}
</script>